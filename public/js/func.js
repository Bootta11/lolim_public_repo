


function initFunctions() {

    //var items_class = document.getElementsByClassName('lolmh_item');

    var onhover_item_function = function () {
        if (IsValidItemId(this.getAttribute('itemid'))) {
            ItemInfo(this.getAttribute('itemid'), this);
        }
    }

    $(".lolmh_item_info_popover").one('mouseenter', function () {
        element = $(this);

        createItemInfoPopover(element);
    });

    function createItemInfoPopover(element) {
        element.attr('hovered', 'true');

        if (!IsValidItemId(element.attr('itemid'))) {
            return;
        }


        $.get("/items/item_info/" + element.attr('itemid'), function (response) {
            var content = '';
            var title = '';

            if (response.description) {
                content = '<div class="item_description_info">' + response.description + '</div>';
            } else {
                content = 'Item info not found';
            }

            if (response.name) {
                title = response.name;
            }

            addPopoverToElement(element, content, title);
        });

    }

    function addPopoverToElement(element, content = '', title = '') {

        element.popover({
            content: content,
            title: title,
            html: true,
            timeout_delay_show_user_info: {show: 500, hide: 200},
            trigger: 'hover'
        });

        if (element.attr('hovered') == 'true') {
            element.popover('show');
        }

        $(element).mouseleave(function () {
            $(this).popover('hide');
        });
    }


    $(".lolmh_item_info_popover").hover(function () {
        $(this).attr('hovered', 'true');
    });

    $(".lolmh_item_info_popover").mouseleave(function () {
        $(this).attr('hovered', '');
    });

    $('.lolmh_matches_div').mouseleave(function () {
        $('.share_match_image_hover').css('display', 'none');
    });

    //Adding show user info on signup
    $("#signup_form #inputSummonerName").focusout(function () {
        user_info_function();
    });

    $("#signup_form #inputRegion").change(function () {
        user_info_function();
    });

    var timeout_summoner_info = null;
    $("#signup_form #inputSummonerName").keyup(function () {

        var input_summoner_name = $(this);

        if (timeout_summoner_info) {
            clearTimeout(timeout_summoner_info);
        }

        timeout_summoner_info = setTimeout(function () {
            if (input_summoner_name.val().length > 0) {
                user_info_function();
            }
        }, 1000);

    });

    var timeout_delay_show_user_info = null;
    function user_info_function() {
        var input_summoner_name = $('#signup_form #inputSummonerName');
        var btn_signup = $('#btn_signup');


        input_summoner_name.attr('validsn', 'false');
        input_summoner_name.popover();
        input_summoner_name.popover('destroy');

        clearTimeout(timeout_delay_show_user_info);

        timeout_delay_show_user_info = setTimeout(showUserInfoPopover, 1000);
    }

    function showUserInfoPopover() {

        var input_summoner_name = $('#signup_form #inputSummonerName');
        var btn_signup = $('#btn_signup');
        var summoner_name = $('#signup_form #inputSummonerName').val();
        var region = $('#signup_form #inputRegion').val();

        var user_info_by_name_url = '/users/user_info_by_name/{{region}}/{{name}}';

        $.get(user_info_by_name_url.replace('{{region}}', region).replace('{{name}}', summoner_name), function (response, text_status, xhr) {
            if (!(xhr.status == 200 || xhr.status == 304)) {
                return;
            }


            var disable_signup_btn = true;

            if (response.name == summoner_name) {
                disable_signup_btn = false;

                createUserInfoPopover(input_summoner_name, response.name, response.profileIconId, response.summonerLevel)
            }

            btn_signup.attr('disabled', disable_signup_btn);

        });
    }

    function createUserInfoPopover(element, summoner_name = '', profile_icon_id = '', summoner_level = '') {
        element.attr('validsn', 'true');
        element.popover({title: "Summoner name: " + summoner_name, trigger: 'manual', content: '<img src="/static/image/profileicon/' + profile_icon_id + '" id="sign_up_icon"><br>LVL: ' + summoner_level, html: 'true'});
        element.popover('show');
    }

    //end adding user info to signup

    //Win-lose popovers
    $('body').tooltip({title: "Win", selector: ".win_ok", trigger: 'hover'});

    $('html').tooltip({title: "Loss", selector: ".win_nok", trigger: 'hover'});


    //Share match
    $('#share_match_div .items_div_share_match .share_match_image_holder').click(function () {
        var el = $(this);

        if (!(IsValidItemId(el.attr('itemid')) && el.attr('itemposition') < 6)) {
            return;
        }

        initItemsOrderList();

        if (window.items_order_list_added_positions.indexOf(el.attr('itemposition')) >= 0) {
            return;
        }

        addItemToItemsOrderList(el)

        RenderOrderList();
    });

    function initItemsOrderList() {

        if (window.items_order_list && window.items_order_list_added_positions) {
            return;
        }

        window.items_order_list = [];
        window.items_order_list_added_positions = [];
    }

    function addItemToItemsOrderList(clicked_item_element) {

        window.items_order_list.push({item_id: clicked_item_element.attr('itemid'), item_position: clicked_item_element.attr('itemposition')});

        var items = [];
        for (var i = 0; i < window.items_order_list.length; i++) {
            items.push("\"" + window.items_order_list[i].item_id + "\"")
        }

        $('#share_match_form #item_order_list').val("[" + items + "]");

        window.items_order_list_added_positions.push(clicked_item_element.attr('itemposition'));

        clicked_item_element.children('.lolmh_item').addClass('img-grey');
    }


    $('#share_match_div .items_div_share_match .share_match_image_holder').mouseenter(function (event) {
        var imghover = $(this).find('.share_match_image_hover');

        $('.share_match_image_hover').css('display', 'none');
        var el = $(this);

        imghover.attr('src', '/images/static/numbers/Numbers-1-icon.png');

        if (window.items_order_list_added_positions) {

            if (!checkShowNextHoverNumber(el)) {
                return;
            }

            imghover.attr('src', '/images/static/numbers/Numbers-' + (window.items_order_list_added_positions.length + 1) + '-icon.png');
        }

        imghover.show();
    });

    function checkShowNextHoverNumber(clicked_item_element) {
        return window.items_order_list_added_positions.indexOf(clicked_item_element.attr('itemposition')) == -1 &&
                clicked_item_element.attr('itemposition') < 6 &&
                IsValidItemId(clicked_item_element.attr('itemid'));
    }

    $('#share_match_div .items_div_share_match .share_match_image_holder').mouseleave(function (event) {

        var imghover = $(this).find('.share_match_image_hover');

        imghover.css('display', 'none');
    });

    $('.share_match_button').click(function (event) {
        event.preventDefault();
        window.items_order_list = [];
        window.items_order_list_added_positions = [];
        var share_button = $(this);

        createShareMatchDialog(share_button)
    });

    function submitShareMatchForm(share_match_form, share_match_button) {
        var url = "/shared_matches"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: $(share_match_form).serialize(), // serializes the form's elements.
            success: function (data)
            {
                if (data.success === true) {
                    share_match_button.replaceWith("<span class='bg-success' style='display: inline-block;padding:4px;margin:0 4px'>Shared</span>");
                    $('#ShareModal').modal('hide');
                }
            }
        });
    }

    function createShareMatchDialog(share_match_button) {
        $.get('/shared_matches/' + $(share_match_button).attr('data-rid'), function (response) {
            var share_match_content = response;

            $('#ShareModal .share-modal-body').html(share_match_content);
            initFunctions();

            $("#share_match_form").submit(function (e) {
                submitShareMatchForm(this, share_match_button)
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });

            $('#ShareModal').modal('show');
        });
    }


    $('.rateit').bind('rated', function (event, value) {
        var match_rid = $(this).attr('data-mrid');
        var rate_element = $(this);
        $.ajax({
            type: "PUT",
            url: '/shared_matches/' + match_rid + '/rate',
            data: {rating: value}, // serializes the form's elements.
            success: function (data)
            {
                if (data.success === true) {
                    rate_element.popover({title: 'Rate', content: 'You rated ' + value, trigger: 'manual'});
                    rate_element.popover('show');
                    var close_rateit_popover_time = setTimeout(function () {
                        $('.rateit').popover('hide');
                    }, 2000);
                }
            }
        });

    });

    $(".lolmh_item_info_popover").popover('hide');

    $('.rateit').mouseenter(function () {
        var rate_element = $(this);
        (function (rating, rate_count, el) {

            el.tooltip({title: rating + " (" + rate_count + " votes)", trigger: 'manual'});
            el.tooltip('show');
            if (hide_tooltip) {
                clearTimeout(hide_tooltip);
            } else {
                var hide_tooltip;
            }
            hide_tooltip = setTimeout(function () {
                el.tooltip('hide');
            }, 1000);
        })($(this).attr('data-rateit-value'), $(this).attr('data-rateit-count'), rate_element)
    });

    function ReloadRateit() {
        $('.rateit').rateit();

    }

    //Change pages on use matches page

    $('#lolmh_matches_div #all_matches_page').change(function () {
        var el = $(this);
        var page_input = $('#lolmh_matches_div #all_matches_page');
        var per_page_input = $('#lolmh_matches_div #all_matches_per_page');
        var page = page_input.val();
        var per_page = per_page_input.val();
        var number_of_pages = parseInt(page_input.attr('data-number-of-pages'), 10);
        var summoner_id = page_input.attr('data-summoner-id');
        if (page == parseInt(page, 10)) {
            page = parseInt(page, 10);
            if (page > number_of_pages) {

                page = number_of_pages;
            }
            if (page < 1) {
                page = 1;
            }
        } else {
            page = number_of_pages;
        }

        if (summoner_id && summoner_id.length > 0) {
            $.ajax({
                type: "GET",
                url: '/users/user_matches_partial/' + summoner_id + '/' + per_page + '/' + page,
                data: {per_page: per_page, page: page}, // serializes the form's elements.
                success: function (data)
                {
                    $('#lolmh_matches_div').html(data);
                    initFunctions();
                    LoadImages();

                }
            });
        }
    });

    $('#lolmh_matches_div #all_matches_per_page').change(function (e) {
        var el = $(this);
        e.preventDefault();
        var value = $('#lolmh_matches_div #navigationBox').attr('data-ref-value');
        var current_page = $('#lolmh_matches_div #current_page').attr('data-current-page');
        var per_page_input = $('#lolmh_matches_div #all_matches_per_page');

        var per_page = per_page_input.val();
        var summoner_id = $('#lolmh_matches_div').attr('data-summoner-id');
        $.ajax({
            type: "GET",
            url: '/user_matches/partial',
            data: {summoner_id: summoner_id, per_page: per_page, refValue: undefined, direction: 'next', page: current_page}, // serializes the form's elements.
            success: function (data)
            {
                $('#lolmh_matches_div').html(data);
                initFunctions();
                LoadImages();

            }
        });
    });

    //end cahnge page on matches page



    $(".lolmh_item").not('#share_match_div .lolmh_item').on("load", function () {
        var el = $(this);
        var img = new Image();
        var data_src = el.attr('data-src');
        if (data_src) {
            img.src = data_src;
            $(img).attr('data-src', el.attr('data-src'));
            img = $(img).addClass(el.attr('class'));
            el.replaceWith(img);
        }
    }).each(function () {

        if (this.complete)
            $(this).load();
    });

    $(".lolmh_champion").not('#share_match_div .lolmh_champion').on("load", function () {
        var el = $(this);
        var img = new Image();
        var data_src = el.attr('data-src');
        if (data_src) {
            img.src = data_src;
            $(img).attr('data-src', el.attr('data-src'));
            img = $(img).addClass(el.attr('class'));
            el.replaceWith(img);
        }
    }).each(function () {

        if (this.complete)
            $(this).load();
    });

    $('#lolmh_matches_div #btnNext').on('click', function (e) {
        e.preventDefault();
        var value = $('#lolmh_matches_div #navigationBox').attr('data-next-ref-value');
        var champion_filter = $('#lolmh_matches_div #champion_filter').val();
        var current_page = $('#lolmh_matches_div #current_page').attr('data-current-page');
        var per_page_input = $('#lolmh_matches_div #all_matches_per_page');

        var per_page = per_page_input.val();
        var summoner_id = $('#lolmh_matches_div').attr('data-summoner-id');

        $.ajax({
            type: "GET",
            url: '/user_matches/partial',
            data: {summoner_id: summoner_id, per_page: per_page, refValue: value, direction: 'next', page: current_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#lolmh_matches_div').html(data);
                initFunctions();
                LoadImages();

            }, error: function (err) {
                console.log("No render data");
            }
        });
    });

    $('#lolmh_matches_div #btnPrev').on('click', function (e) {
        e.preventDefault();
        var value = $('#lolmh_matches_div #navigationBox').attr('data-prev-ref-value');
        var champion_filter = $('#lolmh_matches_div #champion_filter').val();
        ;
        var current_page = $('#lolmh_matches_div #current_page').attr('data-current-page');
        var per_page_input = $('#lolmh_matches_div #all_matches_per_page');

        var per_page = per_page_input.val();
        var summoner_id = $('#lolmh_matches_div').attr('data-summoner-id');
        $.ajax({
            type: "GET",
            url: '/user_matches/partial',
            data: {summoner_id: summoner_id, per_page: per_page, refValue: value, direction: 'prev', page: current_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#lolmh_matches_div').html(data);
                initFunctions();
                LoadImages();

            }, error: function () {
                console.log("No render data");
            }
        });
    });


    $('#lolmh_matches_div #champion_filter').on('change', function (e) {
        e.preventDefault();

        var champion_filter = $('#lolmh_matches_div #champion_filter').val();

        var per_page_input = $('#lolmh_matches_div #all_matches_per_page');

        var per_page = per_page_input.val();
        var summoner_id = $('#lolmh_matches_div').attr('data-summoner-id');

        console.log({summoner_id: summoner_id, per_page: per_page, champion_filter: champion_filter});
        $.ajax({
            type: "GET",
            url: '/user_matches/partial',
            data: {summoner_id: summoner_id, per_page: per_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#lolmh_matches_div').html(data);
                initFunctions();
                LoadImages();

            }, error: function () {
                console.log("No render data");
            }
        });
    });

    // index page

    $('#all-matches-list #btnNext').on('click', function (e) {
        e.preventDefault();
        var value = $('#all-matches-list #navigationBox').attr('data-next-ref-value');
        var champion_filter = $('#all-matches-list #champion_filter').val();
        var current_page = $('#all-matches-list #current_page').attr('data-current-page');
        var per_page_input = $('#all-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page, refValue: value, direction: 'next', page: current_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('.lolmh_center_div #all-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function (err) {
                console.log("No render data");
            }
        });
    });

    $('#all-matches-list #btnPrev').on('click', function (e) {
        e.preventDefault();
        var value = $('#all-matches-list #navigationBox').attr('data-prev-ref-value');
        var champion_filter = $('#all-matches-list #champion_filter').val();
        var current_page = $('#all-matches-list #current_page').attr('data-current-page');
        var per_page_input = $('#all-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page, refValue: value, direction: 'prev', page: current_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('.lolmh_center_div #all-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function () {
                console.log("No render data");
            }
        });
    });


    $('#all-matches-list #champion_filter').on('change', function (e) {
        e.preventDefault();

        var champion_filter = $('#all-matches-list #champion_filter').val();

        var per_page_input = $('#all-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('.lolmh_center_div #all-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function () {
                console.log("No render data");
            }
        });
    });

    $('#all-matches-list #all_matches_per_page').change(function (e) {
        var el = $(this);
        e.preventDefault();

        var champion_filter = $('#all-matches-list #champion_filter').val();
        var per_page_value = el.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page_value, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('.lolmh_center_div #all-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }
        });
    });

    // shared matches page

    $('#shared-matches-list #btnNext').on('click', function (e) {
        e.preventDefault();
        var value = $('#shared-matches-list #navigationBox').attr('data-next-ref-value');
        var champion_filter = $('#shared-matches-list #champion_filter').val();
        var current_page = $('#shared-matches-list #current_page').attr('data-current-page');
        var per_page_input = $('#shared-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();
        var request_data = {per_page: per_page, refValue: value, direction: 'next', page: current_page, champion_filter: champion_filter};

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: request_data, // serializes the form's elements.
            success: function (data)
            {
                $('#shared-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function (err) {
                console.log("No render data");
            }
        });
    });

    $('#shared-matches-list #btnPrev').on('click', function (e) {
        e.preventDefault();
        var value = $('#shared-matches-list #navigationBox').attr('data-prev-ref-value');
        var champion_filter = $('#shared-matches-list #champion_filter').val();
        var current_page = $('#shared-matches-list #current_page').attr('data-current-page');
        var per_page_input = $('#shared-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page, refValue: value, direction: 'prev', page: current_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#shared-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function () {
                console.log("No render data");
            }
        });
    });


    $('#shared-matches-list #champion_filter').on('change', function (e) {
        e.preventDefault();

        var champion_filter = $('#shared-matches-list #champion_filter').val();

        var per_page_input = $('#shared-matches-list #all_matches_per_page');

        var per_page = per_page_input.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#shared-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }, error: function () {
                console.log("No render data");
            }
        });
    });

    $('#shared-matches-list #all_matches_per_page').change(function (e) {
        var el = $(this);
        e.preventDefault();

        var champion_filter = $('#shared-matches-list #champion_filter').val();
        var per_page_value = el.val();

        $.ajax({
            type: "GET",
            url: '/shared_matches/partial',
            data: {per_page: per_page_value, champion_filter: champion_filter}, // serializes the form's elements.
            success: function (data)
            {
                $('#shared-matches-list').html(data);
                initFunctions();
                LoadImages();
                ReloadRateit();
            }
        });
    });
}
;




document.addEventListener("DOMContentLoaded", function (event) {
    initFunctions();

});

function LoadImages() {
    $(".lolmh_champion").not('#share_match_div .lolmh_champion').each(function () {
        var el = $(this);
        var img = new Image();
        var data_src = el.attr('data-src');
        if (data_src) {
            img.src = data_src;
            $(img).attr('data-src', el.attr('data-src'));
            img = $(img).addClass(el.attr('class'));
            el.replaceWith(img);
        }
    });

    $(".lolmh_item").not('#share_match_div .lolmh_item').each(function () {
        var el = $(this);
        var img = new Image();
        var data_src = el.attr('data-src');
        if (data_src) {
            img.src = data_src;
            $(img).attr('data-src', el.attr('data-src'));
            img = $(img).addClass(el.attr('class'));
            el.replaceWith(img);
        }
    });
}

function RenderOrderList() {
    var io_div = $('.items_order_div');
    io_div.html("");
    if (window.items_order_list) {
        for (var i = 0; i < window.items_order_list.length; i++) {

            if (i > 0) {
                io_div.append("<div class='arrow_icon_div' style='display: block'><span>&#8594;</span></div>");
            }


            var img = $('<img>', {src: '/static/image/item/' + window.items_order_list[i].item_id, class: 'lolmh_item_img lolmh_item lolmh_item_order_list'});
            (function (position) {
                var img_div = $('<div>', {class: 'lolmh_item_img_div lolmh_item_info_popover', itemid: window.items_order_list[i].item_id, itemposition: window.items_order_list[i].item_position});
                img_div.css({'position': 'relative'});
                var img_hover = $('<img>', {src: '/images/static/x-mark-16.png'});
                img_hover.css({'display': 'none', position: 'absolute', margin: '8px 0 0 8px', 'background-color': '#fff', top: '0px', left: '0px'});

                img_div.append(img_hover);
                img_div.click(function () {


                    var item_for_removing;

                    item_for_removing = window.items_order_list.filter(function (item) {
                        return item.item_position == position;
                    });

                    window.items_order_list.splice(window.items_order_list.indexOf(item_for_removing[0]), 1);
                    window.items_order_list_added_positions.splice(window.items_order_list_added_positions.indexOf(item_for_removing[0].item_position), 1);
                    $('#share_match_div .share_match_image_holder[itemposition="' + position + '"]').find('.lolmh_item').removeClass('img-grey');
                    RenderOrderList();
                });

                img_div.mouseenter(function () {

                    img_hover.show();


                });

                img_div.mouseleave(function () {

                    img_hover.css('display', 'none');


                });
                img_div.append(img);
                io_div.append(img_div);
            })(window.items_order_list[i].item_position);


        }
    }
}


function IsValidItemId(item_id) {
    var item_id_pattern = /^[0-9]+$/g;
    return item_id_pattern.test(item_id);
}

function fileExists(url) {
    if (url) {
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        return req.status == 200;
    } else {
        return false;
    }
}

function strip(html)
{
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function validateForm() {
    var iusername = $('#signup_form #inputUsername').val();
    var ipassword = $('#signup_form #inputPassword').val();
    var iregion = $('#signup_form #inputRegion').val();
    var iSN = $('#signup_form #inputSummonerName').val();
    var iSNvalid = $('#signup_form #inputSummonerName').attr('validsn');
    if (iusername == "" || ipassword == "" || iregion == "" || iSN == "" || iSNvalid != 'true') {

        return false;
    }
}

function validateLoginForm() {
    var iusername = $('#signup_form #inputUsername').val();
    var ipassword = $('#signup_form #inputPassword').val();

    if (iusername == "" || ipassword == "") {
        return false;
    }
}



(function ($) {
    $.fn.mouseIsOver = function () {
        if ($(this[0]).is(":hover"))
        {
            return true;
        }
        return false;
    };
})(jQuery);

