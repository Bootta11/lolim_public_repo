// Parsing module
var config = require('../config.js');
var express = require('express');
var app = express();
var https = require('https');
var http = require('http');
var redis = require('redis');
var fs = require('fs');
var sqlite3 = require('sqlite3');
var Sqlite_queries = require('./Sqlite_queries.js');
var Redis_functions = require('./Redis_functions.js');
var Main_Tasks = require('./Main_Tasks.js');
var utils = require('./utils/utils.js');
var http = require('http');
var https = require('https');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'PARSING');
var async = require('async');

var path_match = '/api/lol/eune/v2.2/match/[match_id]?api_key=[api_key]';
var path_recent_matches = '/api/lol/{{region}}/v1.3/game/by-summoner/{{summoner_id}}/recent?api_key=RGAPI-d8624126-91b9-4e7c-b0fa-3a108c4a41b6';

var api_key = 'RGAPI-d8624126-91b9-4e7c-b0fa-3a108c4a41b6';
var host = '{{region}}.api.pvp.net';
var client = redis.createClient(); //creates a new client

//SQL-s
var SQL_SELECT_ALL_USERS = 'SELECT * FROM user';

module.exports.ParseAndStoreRecentMatches = ParseAndStoreRecentMatches;
function ParseAndStoreRecentMatches(f_region, f_summoner_id, f_account_id) {
    var api_key = utils.getAuthorizationToken();
    var region_url_mapping = config.regions[f_region].url_mapping;

    log.info('Parsing summoner in region')
    client.get("parse:" + f_region + "_" + f_summoner_id, function (e, d) {
        if (d) {
            log.details("--------------FROM REDIS: User already parsed in last hour = " + d);
        } else {
            log.details("--------------FROM REDIS: Parse dont exist");


            log.info("Parsing recent matches function");
            var request_url = config.urls.api.recent_matches.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key).replace('{{account_id}}', f_account_id);
            log.details('Recent urll: ', request_url);

            var request = https.get(request_url, ParseRecentMatchesData);

            request.on('error', function (err) {
                ret_err = 'ParseAndStoreRecentMatches error: ' + err;

            });

            function ParseRecentMatchesData(response) {
                var data = "";

                var ret_err;
                var json_data;
                if (response.statusCode != '200') {
                    ret_err = "ParseAndStoreRecentMatches - Request error";
                } else {
                    log.details("-----X-Rate-Limit-Count: ", response.headers['x-rate-limit-count']);
                    if (ratelimits) {
                        var ratelimits = response.headers['x-rate-limit-count'].split(",");
                        for (var i = 0; i < ratelimits.length; i++) {
                            Redis_functions.SetValue("ratelimit:" + (ratelimits[i].trim().split(":")[1]), (ratelimits[i].trim().split(":"))[0], -1);
                            //client.set("ratelimit:" + (ratelimits[i].trim().split(":")[1]), (ratelimits[i].trim().split(":"))[0]);
                        }
                    }
                }

                response.on('error', function (err) {
                    ret_err = "Error:" + err;
                });

                response.on('data', function (d) {
                    data += d;
                });

                response.on('end', function () {

                    log.details("Status code: " + response.statusCode);
                    if (response.statusCode == '200' && !ret_err) {
                        json_data = JSON.parse(data);
                        var participant_items = [];

                        //getting participant summoner ids

                        var request_match_data_queue = async.queue(function (task, callback) {
                            log.details('Task requesting match data: ' + task.url);
                            var request_match_data = https.get(task.url, ParseMatchData);

                            request_match_data.on('error', function (err) {
                                ret_err = 'ParseRecentMatchData error: ' + err;
                                log.error(ret_err);
                            });

                            function ParseMatchData(response_match_data) {
                                var match_data = "";
                                var json_match_data;

                                if (response_match_data.statusCode != '200') {
                                    ret_err = "ParseRecentMatchData - Request error";

                                }

                                response_match_data.on('error', function (err) {
                                    reject("Error:", err);
                                });

                                response_match_data.on('data', function (d) {
                                    match_data += d;
                                });

                                response_match_data.on('end', function () {
                                    var json_match_data = '';
                                    try {
                                        json_match_data = JSON.parse(match_data);
                                    } catch (err) {
                                        log.details(json_match_data);
                                        json_match_data = '';
                                    }

                                    if (json_match_data && json_match_data.participantIdentities) {
                                        var summoner_index;
                                        try {
                                            summoner_index = json_match_data.participantIdentities.findIndex(function (part) {
                                                return part.player.summonerId == f_summoner_id;
                                            });

                                            log.details('Summoner id', f_summoner_id);
                                            log.details('Summoner index: ', summoner_index);
                                            var game_id = json_match_data.gameId;
                                            var game_creation_date = json_match_data.gameCreation;
                                            var champion_id = json_match_data.participants[summoner_index].championId;
                                            var stats = json_match_data.participants[summoner_index].stats;

                                            //add match items

                                            var summoner_items = [];
                                            summoner_items.push((stats.item0 ? stats.item0 : -1));
                                            summoner_items.push((stats.item1 ? stats.item1 : -1));
                                            summoner_items.push((stats.item2 ? stats.item2 : -1));
                                            summoner_items.push((stats.item3 ? stats.item3 : -1));
                                            summoner_items.push((stats.item4 ? stats.item4 : -1));
                                            summoner_items.push((stats.item5 ? stats.item5 : -1));
                                            summoner_items.push((stats.item6 ? stats.item6 : -1));

                                            var win = (stats.win == true ? 1 : 0);

                                            Sqlite_queries.InsertMatch(game_id, game_creation_date, f_summoner_id, win, JSON.stringify(summoner_items), champion_id, HandleInsertMatch);

                                            function HandleInsertMatch(err) {
                                                if (err) {
                                                    log.warn(err);
                                                }
                                            }
                                        } catch (err) {
                                            log.details('json_match_data: ', json_match_data);
                                            log.error(err);
                                        }

                                        callback();
                                    } else {
                                        log.warn('Match data is not valid');
                                        callback();
                                    }
                                });
                            }

                        }, 1);

                        for (var g = 0; g < json_data.matches.length; g++) {

                            var match_id = json_data.matches[g].gameId;

                            Main_Tasks.CheckMatchExist(f_summoner_id, match_id, HandleMatchExistInDb);

                            function HandleMatchExistInDb(exist, match_id) {
                                console.log('Match exist: ', exist, ' match_id: ', match_id);
                                
                                if (exist) {
                                    return;
                                }

                                var match_data_request_url = config.urls.api.match_data.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key).replace('{{match_id}}', match_id);
                                log.details('Match data url: ', match_data_request_url);
                                request_match_data_queue.push({url: match_data_request_url}, function (err) {
                                    log.details('Finished: ', match_data_request_url);
                                });
                            }

                        }

                        Redis_functions.SetValue("parse:" + f_region + "_" + f_summoner_id, true, 60 * 60);

                        return;


                        //resolve(ret);
                    } else {
                        if (ret_err) {
                            log.error('Error: ' + ret_err);
                        }

                        if (response.statusCode != '200') {
                            log.error("Error: status code " + response.statusCode);
                        }
                    }
                });
            }






        }
    });
}
;






