// Request and response handlers
var express = require('express');
var app = express();
var config = require('../config.js');
var bodyParser = require('body-parser');
var https = require('https');
var http = require('http');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'REQ_RES_HANDLERS');

http.globalAgent.maxSockets = Infinity;
https.globalAgent.maxSockets = Infinity;

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies


function PreparePaging(request, response, next) {
    var paging = false;
    var page;
    var per_page;
    request.prevValue = undefined;
    request.nextValue = undefined;

    log.details("req method: ", request.method);
    if (request.method.toLowerCase() == 'get') {
        log.details("ITS GET METHOD");
        if (request.params.page && request.params.per_page) {
            paging = true;
            page = request.params.page;
            log.details("Page b: " + page + " parsed " + parseInt(page, 10));
            if (page == parseInt(page, 10)) {
                if (page < 1) {
                    page = 1;
                }
            } else {
                page = 1;
            }

            per_page = request.params.per_page;
            log.details("Per page b: " + per_page + " parsed: " + parseInt(per_page, 10));
            if (per_page == parseInt(per_page, 10)) {
                if (per_page < 4 || per_page > 50) {
                    per_page = 10;
                }
            } else {
                per_page = 10;
            }

            request.paging = paging;
            request.page = page;
            request.per_page = per_page;
            if (!request.firstRowId)
                request.firstRowId = 0;


            log.details("Page: " + page + " per page: " + per_page);
        }
    } else if (request.method.toLowerCase() == 'post') {
        log.details("ITS POST METHOD");
        log.details("refValue: ", request.body.refValue);
        log.details("Per page: ", request.body.per_page);
        if (request.body.refValue || request.body.per_page) {
            paging = true;
            page = request.body.page;
            log.details("Page b: " + page + " parsed " + parseInt(page, 10));
            if (page == parseInt(page, 10)) {
                if (page < 1) {
                    page = 1;
                }
            } else {
                page = 1;
            }

            per_page = request.body.per_page;
            log.details("Per page b: " + per_page + " parsed: " + parseInt(per_page, 10));
            if (per_page == parseInt(per_page, 10)) {
                if (per_page < 4 || per_page > 50) {
                    per_page = 10;
                }
            } else {
                per_page = 10;
            }

            request.paging = paging;
            request.page = page;
            request.per_page = per_page;


            request.refValue = request.body.refValue;
            request.direction = request.body.direction;




            log.details("Page: " + page + " per page: " + per_page);
        }
    }

    if (!paging)
    {
        log.details("No paging");
        request.paging = paging;
        request.page = 1;
        request.per_page = 10;
    }

    next();
}

module.exports.PreparePaging = PreparePaging;
