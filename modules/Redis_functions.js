// Redis functions
var config = require('../config.js');
var redis = require('redis');
var client = redis.createClient(); //creates a new client
var Sqlite_queries = require('./Sqlite_queries.js');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'REDIS_FUNCTIONS');

function SetValue(name, value, expire_in_s) {
    log.info('Setting redis val: ', name);
    try {
        client.set(name + "", value + "");
        if (expire_in_s) {
            client.expire(name, expire_in_s);
        }
    } catch (err) {
        log.error(err);
        callback('');
    }
}
module.exports.SetValue = SetValue;


function GetValue(name, callback) {
    log.info('Redis geting value: ' + name)
    try {
        client.get(name, function (e, d) {
            if (e) {
                log.error("Error: " + e);
                callback('');
            }
            callback(d);
        });
    } catch (err) {
        log.error(err);
        callback('');
    }
}
module.exports.GetValue = GetValue;

function GetUserMatchesCount(userid, callback) {
    var ret_err;
    var ret_count;
    log.info('GetUserMatchesCount');
    client.get('count:user_' + userid + '_matches', function (err, d) {
        if (err) {
            log.error("Error geting fromRedis count:shared_matches_with_rating: ", err);
            ret_err = err;
        } else {
            log.info("No error from redis");
            if (d) {
                log.info("FROM REDIS COUNT SHARD MATCHES: " + d);
                ret_count = parseInt(d, 10);
            } else {

                ret_err = "Not found user matches count";
            }
        }
        callback(ret_err, ret_count);
    });
}
module.exports.GetUserMatchesCount = GetUserMatchesCount;

function GetUserMatchesCountWithChampionFilter(userid, champion_filter, callback) {
    var ret_err;
    var ret_count;
    log.info('GetUserMatchesCountWithChampionFilter');
    client.get('count:user_' + userid + '_matches_champ_filter_' + champion_filter, function (err, d) {
        if (err) {
            log.error("Error geting fromRedis count:" + 'count:user_' + userid + '_matches_champ_filter_' + champion_filter, err);
            ret_err = err;
        } else {
            log.info("No error from redis");
            if (d) {
                log.info("FROM REDIS COUNT SHARD MATCHES WITH FILTER: " + d);
                ret_count = parseInt(d, 10);
            } else {

                ret_err = "Not found user matches count with filter";
            }
        }
        callback(ret_err, ret_count);
    });
}
module.exports.GetUserMatchesCountWithChampionFilter = GetUserMatchesCountWithChampionFilter;

module.exports.GetUserWinCount = GetUserWinCount;
function GetUserWinCount(userid, callback) {
    var ret_err;
    var ret_count;
    log.info('Getting from redis. ' + 'count:user_' + userid + '_win');
    client.get('count:user_' + userid + '_win', function (err, d) {
        if (err) {
            log.error("Error geting fromRedis count:win for user: " + userid, err);
            ret_err = err;
        } else {
            log.info("No error from redis");
            if (d) {
                log.info("FROM REDIS COUNT WIN: " + d);
                ret_count = parseInt(d, 10);
            } else {

                ret_err = "Not found user matches count";
            }
        }
        callback(ret_err, ret_count);
    });
}

module.exports.GetUserWinCountWithChampionFilter = GetUserWinCountWithChampionFilter;
function GetUserWinCountWithChampionFilter(userid, champion_filter, callback) {
    var ret_err;
    var ret_count;
    log.info('Getting from redis. ' + 'count:user_' + userid + '_win');
    client.get('count:user_' + userid + '_win', function (err, d) {
        if (err) {
            log.error("Error geting fromRedis count:win for user: " + userid, err);
            ret_err = err;
        } else {
            log.info("No error from redis");
            if (d) {
                log.info("FROM REDIS COUNT WIN: " + d);
                ret_count = parseInt(d, 10);
            } else {

                ret_err = "Not found user matches count";
            }
        }
        callback(ret_err, ret_count);
    });
}


function GetUserMatchesCountUnified(redis_count, callback) {
    var ret_err;
    var ret_count;
    log.info('GetUserMatchesCountUnified');
    client.get(redis_count, function (err, d) {
        if (err) {
            log.error("Error geting fromRedis count:shared_matches_with_rating: ", err);
            ret_err = err;
        } else {
            log.info("No error from redis");
            if (d) {
                log.info("FROM REDIS COUNT SHARD MATCHES: " + d);
                ret_count = parseInt(d, 10);
            } else {

                ret_err = "Not found user matches count";
            }
        }
        callback(ret_err, ret_count);
    });
}
module.exports.GetUserMatchesCountUnified = GetUserMatchesCountUnified;

module.exports.UserMatchesRender = UserMatchesRender;
function UserMatchesRender(paging, per_page, page, refValue, direction, userid, callback) {
    var ret_err;
    var ret_data;
    log.info('Getting from redis value of: ' + 'render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? '_next' + direction : '') : ''));
    client.get('render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? '_next' + direction : '') : ''), function (err, d) {
        if (err) {
            ret_err = err;
        } else {
            ret_data = d;
        }
        callback(ret_err, ret_data);
    });
}

module.exports.UserMatchesRenderWithChampionFilter = UserMatchesRenderWithChampionFilter;
function UserMatchesRenderWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, callback) {
    var ret_err;
    var ret_data;
    log.info('Getting from redis value of: ' + 'render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? '_next' + direction : '') : '') + ("_champ_filter_" + champion_filter));
    client.get('render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? ('_direction_' + direction) : '') : "") + ('_champ_filter_' + champion_filter), function (err, d) {
        if (err) {
            ret_err = err;
        } else {
            ret_data = d;
        }
        callback(ret_err, ret_data);
    });
}

function UserMatchesRenderUnified(name, paging, per_page, page, callback) {
    var ret_err;
    var ret_data;
    log.info('UserMatchesRenderUnified');
    client.get(name + (paging ? "_p" + page + "_pp" + per_page : ''), function (err, d) {
        if (err) {
            ret_err = err;
        } else {
            ret_data = d;
        }
        callback(ret_err, ret_data);
    });
}
module.exports.UserMatchesRenderUnified = UserMatchesRenderUnified;

module.exports.GetAllChampionsInfoCache = GetAllChampionsInfoCache;
function GetAllChampionsInfoCache(region, callback) {
    var ret_err;
    var ret_data;

    log.info('GetAllChampionsInfoCache');

    GetValue('champions:all_' + region, HandleAllChampionsCache);

    function HandleAllChampionsCache(data) {
        if (data && data.length > 0) {
            ret_data = data;
            callback(ret_err, ret_data);
        } else {
            ret_err = 'Redis cache not found';
            callback(ret_err, ret_data);
        }
    }
}

module.exports.GetAllChampionsIdNameListCache = GetAllChampionsIdNameListCache;
function GetAllChampionsIdNameListCache(region, callback) {
    var ret_err;
    var ret_data;

    log.info('GetAllChampionsIdNameListCache');

    GetValue('champions:all_id_name_list_' + region, HandleAllChampionsCache);

    function HandleAllChampionsCache(data) {
        if (data && data.length > 0) {
            ret_data = data;
            callback(ret_err, ret_data);
        } else {
            ret_err = 'Redis cache not found';
            callback(ret_err, ret_data);
        }
    }
}

module.exports.SharedMatchesCount = SharedMatchesCount;
function SharedMatchesCount(champion_filter, callback) {
    var ret_err;
    var ret_data;
    log.info('SharedMatchesCount');
    client.get('count:shared_matches_with_rating' + (champion_filter ? "_champion_filter_" + champion_filter : ""), function (err, d) {
        if (err) {
            ret_err = err;
            log.error("Error geting fromRedis count:shared_matches_with_rating: ", ret_err);
        } else {
            log.details("No error from redis");
            if (d) {
                log.details("FROM REDIS COUNT SHARD MATCHES: " + d);
                ret_data = parseInt(d, 10);

            } else {
                ret_err = "No shared matches count data";
            }
        }
        callback(ret_err, ret_data);
    });
}

module.exports.SharedMatchesCache = SharedMatchesCache;
function SharedMatchesCache(champion_filter, per_page, page, callback) {
    var ret_err;
    var ret_data;
    log.details("SharedMatchesCache perpage " + per_page + " page " + page);
    client.get('render:shared_matches_with_rating' + (per_page ? "_per_page_" + per_page : "") + (champion_filter ? "_champion_filter_" + champion_filter : "") + ("_page_" + page), function (err, d) {
        if (err) {
            ret_err = err;
            log.error("Error geting fromRedis render:shared_matches_with_rating: ", ret_err);
        } else {
            log.details("No error from redis");
            if (d) {

                ret_data = JSON.parse(d);

            } else {
                ret_err = "No shared matches data";
            }
        }
        callback(ret_err, ret_data);
    });
}


