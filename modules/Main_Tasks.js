// Main tasks
var Redis_functions = require('./Redis_functions.js');
var Sqlite_queries = require('./Sqlite_queries.js');

var https = require('https');
var API_requests = require('./API_requests.js');
var async = require('async');
var config = require('./../config.js');
var SimplogModule = require('./Simplog')
var log = new SimplogModule(config.log_level, 'MAIN_TASKS');

function GetUserMatchesCount(userid, callback) {
    var ret_err;
    var ret_count;
    Redis_functions.GetUserMatchesCount(userid, HandleUserMatchesCount);

    function HandleUserMatchesCount(err, count) {

        if (err) {
            log.error("Error: " + err);
            Sqlite_queries.GetUserMatchesCount(userid, HandleUserMatchesCount);

            function HandleUserMatchesCount(err, count) {

                if (err) {
                    log.error("Error: " + err);
                    ret_err = err;
                } else {
                    log.details("SHARED COUNT: " + count);
                    ret_count = count;
                    Redis_functions.SetValue('count:user_' + userid + '_matches', count, 60 * 5)
                }
                callback(ret_err, ret_count);
            }
        } else {
            log.details("SHARED COUNT: " + count);
            ret_count = count;
            Redis_functions.SetValue('count:user_' + userid + '_matches', count, 60 * 5);
            callback(ret_err, ret_count);
        }

    }
}
module.exports.GetUserMatchesCount = GetUserMatchesCount;

module.exports.GetUserMatchesCountWithChampionFilter = GetUserMatchesCountWithChampionFilter;
function GetUserMatchesCountWithChampionFilter(userid, champion_filter, callback) {
    var ret_err;
    var ret_count;
    Redis_functions.GetUserMatchesCountWithChampionFilter(userid, champion_filter, HandleUserMatchesCount);

    function HandleUserMatchesCount(err, count) {

        if (err) {
            log.error("Error: " + err);
            Sqlite_queries.GetUserMatchesCountWithFilter(userid, champion_filter, HandleUserMatchesCount);

            function HandleUserMatchesCount(err, count) {

                if (err) {
                    log.error("Error on filtered count: " + err);
                    ret_err = err;
                } else {
                    log.details("Filtered COUNT: " + count);
                    ret_count = count;
                    Redis_functions.SetValue('count:user_' + userid + '_matches_champ_filter_' + champion_filter, count, 60 * 5)
                }
                callback(ret_err, ret_count);
            }
        } else {
            log.details("Filtered COUNT: " + count);
            ret_count = count;
            Redis_functions.SetValue('count:user_' + userid + '_matches_champ_filter_' + champion_filter, count, 60 * 5);
            callback(ret_err, ret_count);
        }

    }
}


module.exports.GetUserWinCount = GetUserWinCount;
function GetUserWinCount(userid, callback) {
    var ret_err;
    var ret_count;
    Redis_functions.GetUserWinCount(userid, HandleUserWinCount);

    function HandleUserWinCount(err, count) {

        if (err) {
            log.error("Error: " + err);
            Sqlite_queries.GetUserWinCount(userid, HandleUserWinCountInner);

            function HandleUserWinCountInner(err, count) {

                if (err) {
                    log.error("Error: " + err);
                    ret_err = err;
                } else {
                    log.details("Win COUNT: " + count);
                    ret_count = count;
                    Redis_functions.SetValue('count:user_' + userid + '_win', count, 60 * 5)
                }
                log.details("Returning win count from sql: ", ret_count);
                callback(ret_err, ret_count);
            }
        } else {
            log.details("Win COUNT: " + count);
            ret_count = count;
            Redis_functions.SetValue('count:user_' + userid + '_win', count, 60 * 5);
            callback(ret_err, ret_count);
        }

    }
}

module.exports.GetUserWinCountWithChampionFilter = GetUserWinCountWithChampionFilter;
function GetUserWinCountWithChampionFilter(userid, champion_filter, callback) {
    var ret_err;
    var ret_count;

    Redis_functions.GetUserWinCount(userid, HandleUserWinCount);

    function HandleUserWinCount(err, count) {

        if (err) {
            log.error("Error: " + err);
            Sqlite_queries.GetUserWinCount(userid, HandleUserWinCountInner);

            function HandleUserWinCountInner(err, count) {

                if (err) {
                    log.error("Error: " + err);
                    ret_err = err;
                } else {
                    log.details("Win COUNT: " + count);
                    ret_count = count;
                    Redis_functions.SetValue('count:user_' + userid + '_win', count, 60 * 5)
                }
                log.details("Returning win count from sql: ", ret_count);
                callback(ret_err, ret_count);
            }
        } else {
            log.details("Win COUNT: " + count);
            ret_count = count;
            Redis_functions.SetValue('count:user_' + userid + '_win', count, 60 * 5);
            callback(ret_err, ret_count);
        }

    }
}


function GetUserMatchesCountUnified(redis_count, callback) {
    var ret_err;
    var ret_count;
    Redis_functions.GetUserMatchesCountUnified(redis_count, HandleUserMatchesCount);

    function HandleUserMatchesCount(err, count) {

        if (err) {
            log.error("Error: " + err);
            Sqlite_queries.GetUserMatchesCount(userid, HandleUserMatchesCount);

            function HandleUserMatchesCount(err, count) {

                if (err) {
                    log.error("Error: " + err);
                    ret_err = err;
                } else {
                    log.details("SHARED COUNT: " + count);
                    ret_count = count;
                    Redis_functions.SetValue(redis_count, count, 60 * 5)
                }
                callback(ret_err, ret_count);
            }
        } else {
            log.details("SHARED COUNT: " + count);
            ret_count = count;
            Redis_functions.SetValue(redis_counts, count, 60 * 5);
        }
        callback(ret_err, ret_count);
    }
}
module.exports.GetUserMatchesCountUnified = GetUserMatchesCountUnified;

module.exports.CountAndGetAllUserMatches = CountAndGetAllUserMatches;
function CountAndGetAllUserMatches(paging, per_page, page, refValue, direction, userid, callback) {
    var ret_err;
    var ret_matches;
    var ret_win_count;
    var ret_matches_count;
    CountAllUserMatches(paging, per_page, page, refValue, direction, userid, PrepareUserMatches);


    function PrepareUserMatches(err, matches_count, win_count) {
        if (err) {
            log.error("Error counting user matches");
            ret_err = "Error counting user matches";

        } else {
            ret_matches_count = matches_count;
            ret_win_count = win_count;
            Sqlite_queries.SelectUserMatchesWithUserAndSharingDataAndPaging(paging, per_page, page, matches_count, refValue, direction, userid, HandleSelectedMatches);
        }
    }

    function HandleSelectedMatches(err, data, isEndReached) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            Redis_functions.SetValue('render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? '_next' + direction : '') + (isEndReached ? '_endReached' : '') : ''), JSON.stringify(data), 30);
            ret_matches = data;
        }
        log.details("From CountAndGetAllUserMatches returning: matches count: " + ret_matches_count + " win_count: " + ret_win_count)
        callback(ret_err, ret_matches_count, ret_matches, ret_win_count);
    }


}

module.exports.CountAndGetAllUserMatchesWithChampionFilter = CountAndGetAllUserMatchesWithChampionFilter;
function CountAndGetAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, callback) {
    var ret_err;
    var ret_matches;
    var ret_win_count;
    var ret_matches_count;
    var ret_filtered_count;
    console.log('~~~~CALL2');
    CountAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, PrepareUserMatches);


    function PrepareUserMatches(err, matches_count, win_count, filtered_count) {
        if (err) {
            log.error("Error counting user matches");
            ret_err = "Error counting user matches";

        } else {
            ret_matches_count = matches_count;
            ret_win_count = win_count;
            ret_filtered_count = filtered_count;
            Sqlite_queries.SelectUserMatchesWithUserAndSharingDataAndPagingWithChampionFilter(paging, per_page, page, matches_count, refValue, direction, userid, champion_filter, HandleSelectedMatches);
        }
    }

    function HandleSelectedMatches(err, data, isEndReached) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            if (data) {
                Redis_functions.SetValue('render:user_matches_' + userid + (paging ? "_pp" + per_page + (refValue ? '_ref' + refValue : '') + (direction ? ('_direction_' + direction) : '') : "") + ('_champ_filter_' + champion_filter), JSON.stringify(data), 30);
            }
            ret_matches = data;
        }
        log.details("From CountAndGetAllUserMatches returning: matches count: " + ret_matches_count + " win_count: " + ret_win_count)

        callback(ret_err, ret_matches_count, ret_matches, ret_win_count, ret_filtered_count);
    }


}

module.exports.CountAllUserMatches = CountAllUserMatches;
function CountAllUserMatches(paging, per_page, page, refValue, direction, userid, callback) {
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var ret_win_count;
    var user_matches = [];

    log.details("CountAndGetAllUserMatches Fid: " + refValue + " lid: " + direction);

    async.parallel({
        user_win_count: function (done) {
            GetUserWinCount(userid, done)
        },
        user_matches_count: function (done) {
            GetUserMatchesCount(userid, done)
        }
    }, function (err, results) {
        if (err) {
            ret_err = err;
        } else {
            ret_matches_count = results.user_matches_count;
            ret_win_count = results.user_win_count;
        }
        callback(ret_err, ret_matches_count, ret_win_count);
    });

}

module.exports.CountAllUserMatchesWithChampionFilter = CountAllUserMatchesWithChampionFilter;
function CountAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, callback) {
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var ret_win_count;
    var ret_user_match_count_with_filter;
    var user_matches = [];
    log.details("CountAndGetAllUserMatchesWithFilter Fid: " + refValue + " lid: " + direction);


    async.parallel({
        user_win_count: function (done) {
            GetUserWinCount(userid, done)
        },
        user_matches_count: function (done) {
            GetUserMatchesCount(userid, done)
        },
        filtered_count: function (done) {
            if (champion_filter && champion_filter.length > 0) {
                GetUserMatchesCountWithChampionFilter(userid, champion_filter, done);
            } else {
                done(undefined);
            }
        }
    }, function (err, results) {
        if (err) {
            ret_err = err;
        } else {
            ret_win_count = results.user_win_count;
            ret_matches_count = results.user_matches_count;
            ret_user_match_count_with_filter = results.filtered_count;
        }
        callback(ret_err, ret_matches_count, ret_win_count, ret_user_match_count_with_filter);
    });

}


function CountAndGetAllUserMatchesUnified(sql_matches, sql_count, paging, per_page, page, userid, callback) {
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var user_matches = [];
    GetUserMatchesCount(userid, PrepareUserMatches);


    function PrepareUserMatches(err, count) {
        if (err) {
            log.error("No shared matches count");
            ret_err = "No shared matches count";
        } else {
            ret_matches_count = count;
            Sqlite_queries.SelectAllSharedMatchesWithUserDataAndPaging(paging, per_page, page, userid, HandleSelectedMatches);
        }
    }

    function HandleSelectedMatches(err, data) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            Redis_functions.SetValue('render:user_matches_' + userid + (paging ? "_p" + page + "_pp" + per_page : ''), JSON.stringify(data), 30);
            ret_matches = data;
        }
        callback(ret_err, ret_matches_count, ret_matches);
    }


}
module.exports.CountAndGetAllUserMatchesUnified = CountAndGetAllUserMatchesUnified;
module.exports.PrepareUserMatchesAndMatchesCount = PrepareUserMatchesAndMatchesCount;
function PrepareUserMatchesAndMatchesCount(paging, per_page, page, refValue, direction, userid, callback) {
    log.details("PrepareUserMatchesAndMatchesCount ref: " + refValue + " next: " + direction);
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var ret_win_count;
    Redis_functions.UserMatchesRender(paging, per_page, page, refValue, direction, userid, HandleUserMatchesRender);
    function HandleUserMatchesRender(err, data_matches) {



        if (err) {
            log.error("Error PrepareUserMatchesAndMatchesCount: " + err);
            CountAndGetAllUserMatches(paging, per_page, page, refValue, direction, userid, HandleUserMatchesAndCount);
        } else {
            if (data_matches) {
                ret_matches = JSON.parse(data_matches);
                CountAllUserMatches(paging, per_page, page, refValue, direction, userid, PrepareUserMatches);
                function PrepareUserMatches(err, matches_count, win_count) {
                    if (err) {
                        log.error("No shared matches count");
                        ret_err = "No shared matches count";
                    } else {
                        ret_matches_count = matches_count;
                        ret_win_count = win_count;
                    }
                    callback(ret_err, ret_matches_count, ret_matches, ret_win_count);
                }


            } else {
                CountAndGetAllUserMatches(paging, per_page, page, refValue, direction, userid, HandleUserMatchesAndCount);
            }

        }


    }

    function HandleUserMatchesAndCount(err, count, matches, win_count) {
        if (err) {
            log.error("No shared matches count");
            ret_err = err;
        } else {
            ret_matches_count = count;
            ret_matches = matches;
        }
        log.details("From HandleUserMatchesAndCount returning: matches count: " + ret_matches_count + " win count: " + win_count);
        callback(ret_err, ret_matches_count, ret_matches, win_count);
    }
}


module.exports.PrepareUserMatchesAndMatchesCountWithChampionFilter = PrepareUserMatchesAndMatchesCountWithChampionFilter;
function PrepareUserMatchesAndMatchesCountWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, callback) {
    log.details("PrepareUserMatchesAndMatchesCount with filter ref: " + refValue + " next: " + direction);
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var ret_win_count;
    var ret_matches_with_filter_count;
    Redis_functions.UserMatchesRenderWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, HandleUserMatchesRender);
    function HandleUserMatchesRender(err, data_matches) {



        if (err) {
            log.error("Error PrepareUserMatchesAndMatchesCount: " + err);
            CountAndGetAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, HandleUserMatchesAndCount);
        } else {
            if (data_matches) {
                ret_matches = JSON.parse(data_matches);
                console.log('~~~~CALL1');
                CountAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, PrepareUserMatches);
                function PrepareUserMatches(err, matches_count, win_count, filtered_count) {
                    if (err) {
                        log.details("No shared matches count");
                        ret_err = "No shared matches count";
                    } else {
                        ret_matches_count = matches_count;
                        ret_win_count = win_count;
                        ret_matches_with_filter_count = filtered_count;
                    }
                    callback(ret_err, ret_matches_count, ret_matches, ret_win_count, ret_matches_with_filter_count);
                }


            } else {
                CountAndGetAllUserMatchesWithChampionFilter(paging, per_page, page, refValue, direction, userid, champion_filter, HandleUserMatchesAndCount);
            }

        }


    }

    function HandleUserMatchesAndCount(err, count, matches, win_count, matches_with_filter_count) {
        if (err) {
            log.details("No shared matches count");
            ret_err = err;
        } else {
            ret_matches_count = count;
            ret_matches = matches;
            ret_matches_with_filter_count = matches_with_filter_count;
        }
        log.details("From HandleUserMatchesAndCount returning: matches count: " + ret_matches_count + " win count: " + win_count);
        callback(ret_err, ret_matches_count, ret_matches, win_count, ret_matches_with_filter_count);
    }
}

function PrepareMatchesUnified(paging, per_page, page, userid, sql_matches, sql_count, callback) {
    var ret_err;
    var ret_matches;
    var ret_matches_count;
    var redis_matches = 'render:' + sql_matches;
    var redis_count = 'count:' + sql_count;
    Redis_functions.UserMatchesRenderUnified(redis_matches, paging, per_page, page, HandleUserMatchesRender);
    function HandleUserMatchesRender(err, data_matches) {



        if (err) {
            log.details("Error: " + err);
            CountAndGetAllUserMatches(paging, per_page, page, userid, HandleUserMatchesAndCount);
        } else {
            if (data_matches) {
                ret_matches = JSON.parse(data_matches);
                Redis_functions.GetUserMatchesCount(redis_count, HandleUserMatchesCount);
                function HandleUserMatchesCount(err, data) {
                    if (err) {
                        ret_err = err;
                        log.details(err);
                    } else {
                        if (data) {
                            log.details("FROM REDIS COUNT SHARD MATCHES: " + data_matches);
                            ret_matches_count = parseInt(data, 10);
                        } else {
                            Redis_functions.SetValue(redis_matches, 0, 5 * 60)
                            ret_matches_count = 0;
                        }
                        log.details("-----|||||||||||||||Reds count user matches: " + data);
                    }
                    callback(ret_err, ret_matches_count, ret_matches);
                }
            } else {
                CountAndGetAllUserMatches(paging, per_page, page, userid, HandleUserMatchesAndCount);
            }

        }


    }

    function HandleUserMatchesAndCount(err, count, matches, win_count) {
        if (err) {
            log.details("No shared matches count");
            ret_err = err;
        } else {
            ret_matches_count = count;
            ret_matches = matches;
        }
        callback(ret_err, ret_matches_count, ret_matches, win_count);
    }
}
module.exports.PrepareMatchesUnified = PrepareMatchesUnified;

module.exports.GetAllChampionsInfo = GetAllChampionsInfo;
function GetAllChampionsInfo(region, callback) {
    log.info('GetAllChampionsInfo')
    var ret_err;
    var ret_data;

    var fn = async.seq(
            function ReadChampionsAcquiredValueFromRedis(done) {
                log.info(' Step ReadChampionsAcquiredValueFromRedis');
                Redis_functions.GetValue('champions:all_acquired_' + region, HandleChampionsAcquiredValue);

                function HandleChampionsAcquiredValue(acquired_data) {
                    done(null, acquired_data);
                }
            },
            function AcquireNewDataIfRequired(acquired, done) {
                log.info('Step AcquireNewDataIfRequired');
                if (!acquired) {
                    log.details('Acquire champions info from api');
                    API_requests.RequestAllChampionsData(region, HandleAllChampionsData);

                    var ret_data;
                    function HandleAllChampionsData(err, data) {
                        if (!err) {
                            ret_data = data;
                            Redis_functions.SetValue('champions:all_' + region, data);
                            Redis_functions.SetValue('champions:all_acquired_' + region, 'DONE', 60 * 60 * 24);
                        }
                        done(null);
                    }
                } else {
                    log.details('Champions info already acquired');
                    done(null);
                }
            },
            function ReadChampionsInfoFromRedis(done) {
                log.info('Step ReadChampionsInfoFromRedis');

                Redis_functions.GetAllChampionsInfoCache(region, HandleGetAllChampionsInfoCache);

                function HandleGetAllChampionsInfoCache(err, data) {
                    if (err) {
                        API_requests.RequestAllChampionsData(region, HandleAllChampionsData);
                        function HandleAllChampionsData(err, data) {
                            if (err) {
                                ret_err = err;
                            } else {
                                ret_data = data;
                                Redis_functions.SetValue('champions:all_' + region, data);
                                Redis_functions.SetValue('champions:all_acquired_' + region, 'DONE', 60 * 60 * 24);
                            }
                            done(ret_err, ret_data);
                        }

                    } else {
                        ret_data = data;

                        done(ret_err, ret_data);
                    }
                }

            }

    );

    fn(callback);

//    Redis_functions.GetAllChampionsInfoCache(region, HandleGetAllChampionsInfoCache);
//    function HandleGetAllChampionsInfoCache(err, data) {
//        if (err) {
//            API_requests.RequestAllChampionsData(region, HandleAllChampionsData);
//            function HandleAllChampionsData(err, data) {
//                if (err) {
//                    ret_err = err;
//                } else {
//                    ret_data = data;
//                    Redis_functions.SetValue('champions:all_' + region, data);
//                    Redis_functions.SetValue('champions:all_acquired_' + region, 'DONE', 60 * 60 * 24);
//                }
//                callback(ret_err, ret_data);
//            }
//
//        } else {
//            ret_data = data;
//
//            callback(ret_err, ret_data);
//        }
//    }
}

module.exports.GetSummonerChamopionsWithNames = GetSummonerChamopionsWithNames;
function GetSummonerChamopionsWithNames(region, userid, callback) {
    var ret_err;
    var ret_data;

    async.parallel({
        summoner_champions_ids: function (callback) {
            Sqlite_queries.SelectSummonerChampions(userid, callback)
        },
        all_champion_data: function (callback) {
            GetAllChampionsInfo(region, callback)
        }

    }, function (err, results) {
        if (err) {
            log.error("ERROR GetSummonerChamopionsWithNames: ", err);
            ret_err = err;
        } else {

            var s_ids = results.summoner_champions_ids;
            var champions_data = JSON.parse(results.all_champion_data);
            ret_data = []
            for (var i = 0; i < s_ids.length; i++) {
                //log.details("ID " + s_ids[i].champion_id + ", name " + champions_data.data[s_ids[i].champion_id].name);
                ret_data.push({id: s_ids[i].champion_id, name: champions_data.data[s_ids[i].champion_id].name});
            }
        }
        //log.details("ret_data: ", ret_data)
        ret_data.sort(function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
        callback(ret_err, ret_data);
    });
}

module.exports.GetAllChampionsIdNameList = GetAllChampionsIdNameList;
function GetAllChampionsIdNameList(region, callback) {
    var ret_err;
    var ret_data;
    console.log('~~~~~~GetAllChampionsIdNameList');
    var fn = async.seq(
            function (done) {
                Redis_functions.GetAllChampionsIdNameListCache(region, function (err, data) {
                    done(null, data);
                });
            },
            function (data, done) {
                Redis_functions.GetValue('champions:all_id_name_list_acquired_' + region, function (value) {
                    done(null, data, value);
                });
            },
            function (data, acquired, done) {
                if (data && acquired) {
                    done(null, data);
                } else {
                    GetAllChampionsInfo(region, function (err, results) {
                        if (err) {
                            log.error("ERROR GetAllChamopionsWithNames: ", err);
                            ret_err = err;
                            if (data) {
                                done(null, data);
                            } else {
                                done("Unable get chapions info");
                            }
                        } else {
                            var champions_data = JSON.parse(results).data;
                            ret_data = [];

                            for (var c in champions_data) {
                                ret_data.push({id: champions_data[c].id, name: champions_data[c].name});
                            }


                            ret_data.sort(function (a, b) {
                                if (a.name < b.name)
                                    return -1;
                                if (a.name > b.name)
                                    return 1;
                                return 0;
                            });

                            Redis_functions.SetValue('champions:all_id_name_list_' + region, JSON.stringify(ret_data));
                            Redis_functions.SetValue('champions:all_id_name_list_acquired_' + region, 'DONE', 60 * 60);
                            done(null, JSON.stringify(ret_data));
                        }

                    });
                }
            },
            function (data, done) {
                if (data) {
                    log.info('GetAllChampionsIdNameList returning data');
                    done(null, data);
                } else {
                    done("No champions id name list");
                }
            });
    fn(callback);
}

module.exports.GetSharedMatches = GetSharedMatches;
function GetSharedMatches(paging, per_page, page, refValue, direction, champion_filter, callback) {
    log.details("GetSharedMatches Paging: " + paging + " per_page: " + per_page + " page: " + page + " refValue: " + refValue + " direction: " + direction + " champ_filter: " + champion_filter);
    var ret_err;

    var fn = async.seq(
            function (done) {
                log.details("FIRt line :", champion_filter);
                Redis_functions.SharedMatchesCount(champion_filter, function (err, data) {
                    done(null, data);
                });
            },
            function (data, done) {
                log.details("SharedMatchesCount champion filter: ", champion_filter);
                log.details('Data count from redis: ', data);
                if (data) {
                    done(null, data);
                } else {
                    Sqlite_queries.SharedMatchesCount(champion_filter, function (err, data) {
                        done(null, data)
                    });
                }
            },
            function (data, done) {
                log.details("Data count third: ", data);
                var page_count;
                if (data) {
                    var page_count = Math.ceil(data / per_page);
                    if (page_count == 0)
                        page_count = 1;

                    if (page > page_count)
                        page = page_count;

                    Redis_functions.SharedMatchesCache(champion_filter, per_page, page, function (err, matches) {
                        log.details("err: ", err);
                        log.details("Data: ", data);
                        done(null, matches, data, page_count, page);
                    });
                } else {
                    done('No data for matches count');
                }

            },
            function (data, count, data_page_count, data_page, done) {
                log.details("Last champion filter: ", champion_filter);
                if (data) {
                    log.details("From redis shared matches leng: ", data.length);
                    done(null, count, data, data_page_count, data_page);
                } else {
                    Sqlite_queries.SelectMatchesWithRating(paging, per_page, data_page, data, refValue, direction, champion_filter, function (err, matches) {
                        Redis_functions.SetValue('render:shared_matches_with_rating' + (per_page ? "_per_page_" + per_page : "") + (champion_filter.length > 0 ? "_champion_filter_" + champion_filter : "") + ("_page_" + data_page), JSON.stringify(matches), 30);
                        done(null, count, matches, data_page_count, data_page);
                    });
                }
            }


    );

    fn(callback);
}

module.exports.CheckForUpdateSummonerData = CheckForUpdateSummonerData;
function CheckForUpdateSummonerData(region, summoner_id, callback) {

    var ret_err;

    var fn = async.seq(
            function (done) {
                log.details("Requesting summoner data from api");

                API_requests.RequestSummonerData(region, summoner_id, function (err, data) {
                    if (err) {
                        done(err);
                    } else {
                        done(null, data);
                    }
                });


            },
            function (data, done) {
                log.details("SharedMatchesCount summoner id: ", summoner_id);
                log.details('Summoner data from api: ', data);
                if (data) {
                    Sqlite_queries.UpdateSummonerData(data, function (err, data) {
                        done(null);
                    });
                } else {
                    done('Missing data from api request');
                }
            }


    );

    fn(callback);
}

module.exports.GetItemsData = GetItemsData;
function GetItemsData(region, callback) {
    log.info('GetItemsData');
    var ret_err;
    var ret_data;

    var fn = async.seq(
            function ReadItemsDataAcquiredValueFromRedis(done) {
                log.info(' Step ReadItemsDataAcquiredValueFromRedis');
                Redis_functions.GetValue("items_data_acquired_" + region, HandleItemsDataAcquiredValue);

                function HandleItemsDataAcquiredValue(acquired_data) {
                    done(null, acquired_data);
                }
            },
            function AcquireNewItemsDataIfRequired(acquired, done) {
                log.info('Step AcquireNewItemsDataIfRequired');
                if (!acquired) {
                    log.details('Acquire items data from api');
                    API_requests.RequestItemsData(region, HandleItemsData);

                    var ret_data;
                    function HandleItemsData(err, data) {
                        if (!err) {
                            ret_data = data;
                            Redis_functions.SetValue("items_data_" + region, data);
                            Redis_functions.SetValue("items_data_acquired_" + region, 'DONE', 60 * 60 * 24);
                        }
                        done(null);
                    }
                } else {
                    log.details('Items data already acquired');
                    done(null);
                }
            },
            function ReadItemsDataFromRedis(done) {
                log.info('Step ReadItemsDataFromRedis');

                Redis_functions.GetValue("items_data_" + region, HandleItemsDataFromRedis);

                function HandleItemsDataFromRedis(data) {
                    if (!data) {
                        API_requests.RequestItemsData(region, HandleItemsData);
                        function HandleItemsData(err, data) {
                            if (err) {

                                ret_err = err;
                            } else {
                                ret_data = data;
                                Redis_functions.SetValue("items_data_" + region, data);
                                Redis_functions.SetValue("items_data_acquired_" + region, 'DONE', 60 * 60 * 24);
                            }
                            done(ret_err, ret_data);
                        }

                    } else {
                        ret_data = data;

                        done(ret_err, ret_data);
                    }
                }

            }

    );

    fn(callback);
}

module.exports.CheckMatchExist = CheckMatchExist;
function CheckMatchExist(summoner_id, match_id, callback) {
    Sqlite_queries.SelectMatchBySummonerIdAndMatchId(summoner_id, match_id, HandleMatchExistInDb);

    function HandleMatchExistInDb(err, data) {
        var ret = false;

        if (!err && data) {
            ret = true;
        }

        callback(ret, match_id);
    }
}
