//API requests
var utils_functions = require('./Util_functions.js');
let utils = require('./utils/utils.js');
var fs = require('fs');
var config = require('../config.js');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'API_REQUESTS');
var http = require('http');
var https = require('https');
http.globalAgent.maxSockets = Infinity;
https.globalAgent.maxSockets = Infinity;

module.exports.RequestCurrentVersion = RequestCurrentVersion;
function RequestCurrentVersion(region, callback) {
    log.info('RequestCurrentVersion');
    var ret_err;
    var ret_data;

    var region_url_mapping = config.regions[region].url_mapping;
    var api_key = utils.getAuthorizationToken();

    var request_url = config.urls.static.versions.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key);
    log.details('Version url: ', request_url);

    var request = https.get(request_url, ParseVersionsData);

    request.on('error', function (err) {
        log.error(err);
        ret_err = "Unable to get current version";

        callback(ret_err, ret_data);
    });

    function ParseVersionsData(response) {
        var data = "";

        response.on('data', function (d) {
            data += d;
        });

        response.on('end', function () {
            if (data && (json_data = JSON.parse(data)) && json_data.length>0) {
                ret_data = json_data[0];
            }else{
                ret_err = "Version data wrong format";
            }

            callback(ret_err, ret_data);
        });
    }
}

module.exports.RequestItemInfo = RequestItemInfo;
function RequestItemInfo(region, id, callback) {
    var region_url_mapping = config.regions[region].url_mapping;
    var api_key = utils.getAuthorizationToken();

    var request_url = config.urls.static.item.replace('{{region}}', region_url_mapping).replace('{{id}}', id).replace('{{api_key}}', api_key);

    var request = https.get(request_url, ParseItemInfoData);
    request.on('error', function (err) {
        log.error(err);
        callback("");
    });

    function ParseItemInfoData(response) {
        var data = "";

        response.on('data', function (d) {
            data += d;
        });

        response.on('end', function () {

            log.info("Parsed item info data: ", JSON.parse(data));
            callback(data);
        });
    }
}

module.exports.RequestItemsData = RequestItemsData;
function RequestItemsData(region, callback) {
    log.info('RequestItemsData');
    utils_functions.GetLatestVersion(region, function (err, data) {
        if (err) {
            var err_msg = 'Get items data nable to get current versions list';
            log.error(err_msg);
            callback(err_msg);
        } else {
            var latest_version = data;

            var request_url = config.urls.static.items.replace('{{version}}', latest_version);

            var request = https.get(request_url, ParseItemsData);
            request.on('error', function (err) {
                ret_err = err;
                callback(ret_err);
            });

            function ParseItemsData(response) {
                var data = "";
                if (response.statusCode != '200' && !ret_err) {
                    ret_err = "Get items data - Request error"
                    return callback(null)
                }

                response.on('data', function (d) {
                    data += d;
                });

                response.on('end', function () {
                    ret_data = data;

                    return callback(null, ret_data);
                });
            }

        }
    });
}

module.exports.RequestAllChampionsData = RequestAllChampionsData;
function RequestAllChampionsData(region, callback) {
    log.info('RequestAllChampionsData');

    var ret_err;
    var ret_data;

    var region_url_mapping = config.regions[region].url_mapping;
    var api_key = utils.getAuthorizationToken();

    var request_url = config.urls.static.champions.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key);
    var request = https.get(request_url, ParseAllChampionData);
    request.on('error', function (err) {
        ret_err = err;
        callback(ret_err, ret_data);
    });

    function ParseAllChampionData(response) {
        var data = "";
        console.log('~~~~~~ region: ',region, ' request url: ', request_url);
        if (response.statusCode != '200' && !ret_err) {
            ret_err = "Champions data - Request error. Status code: " + response.statusCode;
            return callback(ret_err, ret_data);
        }

        response.on('data', function (d) {
            data += d;
        });

        response.on('end', function () {
            ret_data = data;
            return callback(ret_err, ret_data);
        });
    }

}


module.exports.RequestSummonerData = RequestSummonerData;
function RequestSummonerData(region, summoner_id, callback) {
    log.info('RequestSummonerData');
    log.details(`Getting summoner ${summoner_id} data from regionn ${region}`);
    var ret_err;
    var ret_data;


    var region_url_mapping = config.regions[region].url_mapping;
    var api_key = utils.getAuthorizationToken();

    var request_url = config.urls.api.user_data_by_summoner_id.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key).replace('{{summoner_id}}', summoner_id);
    log.info(request_url);
    var request = https.get(request_url, ParseSummonerData);

    request.on('error', function (err) {
        ret_err = 'RequestSummonerData error: ' + err;
        callback(ret_err, ret_data);
    });

    function ParseSummonerData(response) {
        var data = "";
        if (response.statusCode != '200' && !ret_err) {
            ret_err = "RequestSummonerData - Request error";
            return callback(ret_err, ret_data);
        }

        response.on('data', function (d) {
            data += d;
        });

        response.on('end', function () {
            ret_data = data;
            return callback(ret_err, ret_data);
        });
    }


}

module.exports.RequestSummonerDataByName = RequestSummonerDataByName;
function RequestSummonerDataByName(region, summoner_name, callback) {
    log.info('RequestSummonerDataByName');
    log.details(`Getting summoner data by name ${summoner_name} from regionn ${region}`);
    var ret_err;
    var ret_data;


    var region_url_mapping = config.regions[region].url_mapping;
    var api_key = utils.getAuthorizationToken();

    var request_url = config.urls.api.summoner_data_by_name.replace('{{region}}', region_url_mapping).replace('{{api_key}}', api_key).replace('{{summoner_name}}', summoner_name);
    log.info(request_url);
    var request = https.get(request_url, ParseSummonerData);

    request.on('error', function (err) {
        ret_err = 'RequestSummonerDataByName error: ' + err;
        callback(ret_err, ret_data);
    });

    function ParseSummonerData(response) {
        var data = "";
        if (response.statusCode != '200' && !ret_err) {
            ret_err = "RequestSummonerDataByName - Request error";
            return callback(ret_err, ret_data);
        }

        response.on('data', function (d) {
            data += d;
        });

        response.on('end', function () {
            ret_data = data;
            return callback(ret_err, ret_data);
        });
    }


}




