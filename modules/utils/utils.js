let config = require('../../config.js');

function getMinutesFlooredToClosestTens() {
    var coeff = 1000 * 60 * 10;
    var date = new Date(); //or use any other date
    var rounded = new Date(Math.floor(date.getTime() / coeff) * coeff);
    var stamp = rounded.getHours() + "" + rounded.getMinutes();
    return stamp;
}

function escapeHtml(unsafe) {
    return unsafe;
    return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
}

function getAuthorizationToken() {
    return config.api_token;
}

module.exports = {
    getMinutesFlooredToClosestTens: getMinutesFlooredToClosestTens,
    escapeHtml: escapeHtml,
    getAuthorizationToken: getAuthorizationToken
};