// users
let express = require('express');
let router = express.Router();
let users = require('./users.js');
let paging = require('../paging/paging.js');
let security = require('../security/security.js');
let matches = require('../matches/matches.js');

router.get('/',security.verifyJWTToken, users.loadUsersDataFromSession, users.renderUsersPage);
router.get('/get_by_id/:region/:userid', security.verifyJWTToken, users.loadUsersDataFromSession, paging.preparePaging, matches.getUserMatches, users.renderUserMatchesPage);
router.get('/logged_user_matches',security.verifyJWTToken, users.loadUsersDataFromSession, paging.preparePaging, matches.getUserMatches, users.renderLoggedUserMatchesPage);
router.get('/user_info_by_name/:region/:name', users.getUserInfoByName);
router.get('/user_matches_partial/:userid/:per_page/:page', users.renderUserMatchesPartial);
router.all('/signup', users.loadUsersDataFromSession, users.userSignup);
router.all('/login', users.loadUsersDataFromSession, users.userLogin);
router.all('/logout', users.userLogout);
router.post('/user_signup_process', users.userSignupProcess);
router.post('/user_authenticate', users.userAuthenticate);

module.exports = router;