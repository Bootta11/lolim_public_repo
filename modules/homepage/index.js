// homepage
let express = require('express');
let router = express.Router();
let homepage = require('./homepage.js');
let users = require('../users/users.js');
let paging = require('../paging/paging.js');
let security = require('../security/security.js');
let matches = require('../matches/matches.js')

router.all('/', users.loadUsersDataFromSession, paging.preparePaging,matches.getTopMatches, matches.getAllSharedMatches,  homepage.renderHomepage);

module.exports = router;