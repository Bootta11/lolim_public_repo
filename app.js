const config = require('./config.js');
const config_validation_schema = require('./config_validation_schema.js');
const config_validator = require('./modules/config_validator/config_validator.js');
config_validator.validateConfigFile(config, config_validation_schema)

const express = require('express');
const app = express();
const https = require('https');
const http = require('http');
const bodyParser = require('body-parser');
const session = require('express-session');
const Sqlite_queries = require('./modules/Sqlite_queries.js');
const Scheduling_functions = require('./modules/Scheduling_functions.js');
const SimplogModule = require('./modules/Simplog.js');
const log = new SimplogModule(config.log_level, 'APP_JS');
const users = require('./modules/users/users.js');
const paging = require('./modules/paging/paging.js');
const security = require('./modules/security/security.js');
const matches = require('./modules/matches/matches.js');
const utils = require('./modules/utils/utils.js');
const static_files = require('./modules/static_files/static_files.js');

http.globalAgent.maxSockets = Infinity;
https.globalAgent.maxSockets = Infinity;

var lolim_session = session({secret: 'testbot',
    resave: false,
    saveUninitialized: true,
    cookie: {}
});

if (app.get('env') === 'production') {
    app.set('trust proxy', 1); // trust first proxy
    lolim_session.cookie.secure = true; // serve secure cookies
}

app.use(lolim_session);
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

// setting template engine
app.set('view engine', 'ejs');
app.use(express.static('public'));

//load modules routers
let homepage_router = require('./modules/homepage/index.js');
let users_router = require('./modules/users/index.js');
let static_router = require('./modules/static_files/index.js');
let matches_router = require('./modules/matches/index.js');
let items_router = require('./modules/items/index.js')

app.use('/', homepage_router);
app.use('/users', users_router);
app.use('/static', static_router);
app.use('/', matches_router);
app.use('/items', items_router);


let server = app.listen(config.port, function () {

    log.info('Example app listening on port ' + (process.argv[2] ? process.argv[2] : 3000) + '! Env: ' + app.get('env'));

    Sqlite_queries.InitDB();

    static_files.minifyJS();
    
    Scheduling_functions.StartSchedulingJob();
});

module.exports = server;




