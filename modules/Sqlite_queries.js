// Sqlite queries
var config = require('../config.js');
var sqlite3 = require('sqlite3');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'SQLITE_QUERIES');
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
var db;

//create table sql-s
var SQL_CREATE_MATCH_TABLE = 'CREATE TABLE IF NOT EXISTS match (match_id INTEGER, create_date INTEGER, summoner_id TEXT, win INTEGER, items TEXT, champion_id TEXT, CONSTRAINT unq UNIQUE (match_id, summoner_id));';
var SQL_CREATE_USER_TABLE = 'CREATE TABLE IF NOT EXISTS user (username TEXT, password, summoner_id TEXT UNIQUE, icon_id TEXT, region TEXT, role TEXT)';
var SQL_CREATE_RATING_TABLE = 'CREATE TABLE IF NOT EXISTS rating (user_summoner_id TEXT, match_rowid TEXT, create_time TEXT, change_time TEXT, rating REAL)';
var SQL_CREATE_SHARE_MATCH_TABLE = 'CREATE TABLE IF NOT EXISTS share_match (match_rowid TEXT UNIQUE, create_time TEXT, item_order TEXT)';
//count sql-s
var SQL_COUNT_USER_MATCHES = 'SELECT count(*) as matches_count FROM(SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=?) msh  join user u ON msh.summoner_id = u.summoner_id)';
var SQL_COUNT_USER_MATCHES_WITH_CHAMPION_FILTER = 'SELECT count(*) as matches_count FROM(SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=?) msh  join user u ON msh.summoner_id = u.summoner_id where champion_id = ?)';
var SQL_COUNT_USER_WIN = 'SELECT count(*) as matches_count FROM(SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=?) msh  join user u ON msh.summoner_id = u.summoner_id) WHERE win > 0';
var SQL_COUNT_SHARED_MATCHES_WITH_RATING = 'SELECT count(share_id) as matches_count FROM (SELECT * FROM (SELECT match_share.*,AVG(r.rating) AS rating,count(r.rating) as rate_count, r.change_time  AS rating_change_time FROM (SELECT *,m.rowid AS id, sh.rowid as share_id FROM match m  JOIN share_match sh ON m.rowid=sh.match_rowid) match_share left JOIN rating r ON match_share.id=r.match_rowid GROUP BY match_share.match_rowid ORDER BY match_share.share_id DESC){{filter_block}});';
//select sql-s
var SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA = 'SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ORDER BY m.create_date DESC';
var SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING = 'SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ORDER BY m.create_date DESC) msh  join user u ON msh.summoner_id = u.summoner_id LIMIT ? OFFSET ?';
var SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_GREATER_optimized = 'SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ORDER BY m.create_date DESC) msh  join user u ON msh.summoner_id = u.summoner_id where create_date < ? {{ADD_WHERE_OPTION}} limit ?;';
var SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_LOWER_optimized = 'SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ORDER BY m.create_date ASC) msh  join user u ON msh.summoner_id = u.summoner_id where msh.create_date >= ? {{ADD_WHERE_OPTION}} limit ?;';
var SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_FIRST_PAGE_optimized = 'SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ORDER BY m.create_date DESC) msh  join user u ON msh.summoner_id = u.summoner_id {{ADD_WHERE}} limit ?;';
var SQL_SELECT_MATCH_BY_ROW_ID = 'SELECT *, rowid AS id  FROM match WHERE id= ? ORDER BY create_date DESC';
var SQL_SELECT_SUMMONER_CHAMPIONS = 'SELECT champion_id FROM match WHERE summoner_id = ? GROUP BY champion_id;';
var SQL_SELECT_MATCH_WITH_RATING_BY_USER_ID_AND_MATCH_ROW_ID = 'SELECT *, rowid as rowid FROM rating WHERE user_summoner_id=? and match_rowid=?';
//update sql-s
var SQL_UPDATE_RATING_BY_SUMMONER_ID_AND_MATCH_ROWID = 'UPDATE rating SET rating=? WHERE user_summoner_id=? and match_rowid=?';
var SQL_INSERT_MATCH_RATING = 'INSERT INTO rating (user_summoner_id, match_rowid, create_time, change_time, rating) VALUES (?, ?, ?, ?, ?)';
var SQL_SELECT_MATCH_BY_SUMMONER_ID_AND_MATCH_ID = 'SELECT * FROM match WHERE summoner_id = ? and match_id = ?';
var SQL_INSERT_NEW_USER_DATA = 'INSERT INTO user VALUES (?, ?, ?, ?, ?, ?, ?)';

module.exports.CloseDB = CloseDB;
function CloseDB() {
    db.close();
}

module.exports.InitDB = InitDB;
function InitDB() {
    var db_path = config.sqlite_db;
    var db_dir = path.dirname(db_path);
    if (!fs.existsSync(db_dir)) {
        log.details('Sqlite db dir not exists. Making folder.');
        mkdirp.sync(db_dir);
    }

    db = new sqlite3.Database(db_path);
    db.serialize(function () {
        //db.run('DROP TABLE match');
        db.run(SQL_CREATE_MATCH_TABLE);
        db.run(SQL_CREATE_USER_TABLE);
        db.run(SQL_CREATE_SHARE_MATCH_TABLE);
        db.run(SQL_CREATE_RATING_TABLE);

        addColumnToTableIfNotExist('account_id', 'user');


    });
}
module.exports.UpdateSummonerData = UpdateSummonerData;
function UpdateSummonerData(data, callback) {
    log.details('Updating summener data ', data);
    var summoner_data = JSON.parse(data);

    var sql = 'UPDATE user SET account_id = ?, icon_id = ? WHERE summoner_id = ?';
    db.run(sql, [summoner_data.accountId, summoner_data.profileIconId, summoner_data.id], function (err) {
        if (err) {
            log.error('UpdateSummonerData error: ', err);
        } else {
            log.info('Summoner data updated in database');
        }
    });
}

function addColumnToTableIfNotExist(column, table) {
    log.details(`Add column ${column} to table ${table} if not exist`);
    db.all('PRAGMA table_info(' + table + ')', function (err, rows) {
        if (err) {
            log.error('addColumnToTableIfNotExist error: ' + err);
        } else {
            if (rows) {
                var columns = rows.map(function (o) {
                    return o.name;
                });

                if (columns.includes(column)) {
                    log.info(`Column ${column} already exist in table ${table}`);
                } else {
                    addColumnToTable(column, table);
                }
            } else {
                log.warn("Unable to get columns info");
            }

        }
    });
}

function addColumnToTable(column, table) {
    db.run('ALTER TABLE ' + table + ' ADD COLUMN ' + column + ' TEXT', function (err) {
        if (err) {
            log.error('Add column to table error: ', err);
        } else {
            log.info('Successfully added new column ' + column + ' to table ' + table);
        }
    });
}

module.exports.GetUserMatchesCount = GetUserMatchesCount;
function GetUserMatchesCount(userid, callback) {
    var ret_err;
    var ret_matches_count;
    db.get(SQL_COUNT_USER_MATCHES, [userid], function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            if (row) {
                ret_matches_count = row.matches_count;
            } else {
                log.warn("No shared matches count");
                ret_err = "No shared matches count";
            }

        }
        callback(ret_err, ret_matches_count);
    });
}

module.exports.GetUserMatchesCountWithFilter = GetUserMatchesCountWithFilter;
function GetUserMatchesCountWithFilter(userid, champion_filter, callback) {
    var ret_err;
    var ret_matches_count;
    db.get(SQL_COUNT_USER_MATCHES_WITH_CHAMPION_FILTER, [userid, champion_filter], function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            if (row) {
                ret_matches_count = row.matches_count;
            } else {
                log.warn("No shared matches count");
                ret_err = "No shared matches count";
            }

        }
        callback(ret_err, ret_matches_count);
    });
}

module.exports.GetUserWinCount = GetUserWinCount;
function GetUserWinCount(userid, callback) {
    var ret_err;
    var ret_matches_count;
    db.get(SQL_COUNT_USER_WIN, [userid], function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        } else {
            if (row) {
                ret_matches_count = row.matches_count;
            } else {
                log.warn("No shared matches count");
                ret_err = "No shared matches count";
            }

        }
        callback(ret_err, ret_matches_count);
    });
}


module.exports.SelectUserMatchesWithUserAndSharingDataAndPaging = SelectUserMatchesWithUserAndSharingDataAndPaging;
function SelectUserMatchesWithUserAndSharingDataAndPaging(paging, per_page, page, rows_count, refValue, direction, userid, callback) {
    var ret_err;
    var ret_data;
    var matches_list = [];
    log.info("SelectUserMatchesWithUserAndSharingDataAndPaging Fid: " + refValue + " lid: " + direction + ' per page: ' + per_page);
    //db.each(SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING, (paging ? [userid, per_page, (page - 1) * per_page] : [userid, 10, 0]), function (err, row) {
    var sql_query = "";
    var params;
    if (!refValue || !direction) {
        sql_query = SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_FIRST_PAGE_optimized.replace("{{ADD_WHERE}}", "");
        params = [userid, per_page];
    } else {
        if (direction) {
            log.info('direction val: ' + direction);
            if (direction == 'next') {
                log.info('Direction is next');
                params = [userid, refValue, per_page]
                sql_query = SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_GREATER_optimized.replace("{{ADD_WHERE_OPTION}}", "");
            } else if (direction == 'endRows') {
                log.info('end rows');
                params = [userid, refValue, per_page]
                sql_query = SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_LOWER_optimized.replace("{{ADD_WHERE_OPTION}}", "");
            } else {
                log.info('Direction is prev');
                params = [userid, refValue, per_page * 2]
                sql_query = SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_LOWER_optimized.replace("{{ADD_WHERE_OPTION}}", "");
            }
        } else {
            log.info('direction undefined');
            params = [userid, refValue, per_page]
            sql_query = SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING_LOWER_optimized.replace("{{ADD_WHERE_OPTION}}", "");
        }
    }
    db.each(sql_query, params, function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        }
        log.info("From database: " + row.id + ': ' + row.match_id, row.create_date, row.summoner_id, JSON.parse(row.items));
        //row.match_details = JSON.parse(row.match_details);
        matches_list.push(row);
    }, function () {

        log.info("Geting all sahred users with rating -----COMPLETE---- ", matches_list.length);
        var isEndReached = false;
        if (direction && direction == 'prev') {
            matches_list.reverse();
            log.info('Page is ' + page);
            var page_count = Math.ceil((rows_count) / per_page)
            log.info('Page count in sql is ' + page_count);
            if (direction == 'endRows') {

            } else {
                //ret_data = matches_list.slice(per_page*(-1));
                //ret_data = matches_list.slice(per_page * (-1));
                ret_data = matches_list.slice(0, per_page);
            }
        } else if (direction && direction == 'endRows') {
            matches_list.reverse();
            log.info('End rows');
            ret_data = matches_list;
            isEndReached = true;
        } else {
            ret_data = matches_list;
        }
        callback(ret_err, ret_data, isEndReached);
    });
}

module.exports.SelectUserMatchesWithUserAndSharingDataAndPagingWithChampionFilter = SelectUserMatchesWithUserAndSharingDataAndPagingWithChampionFilter;
function SelectUserMatchesWithUserAndSharingDataAndPagingWithChampionFilter(paging, per_page, page, rows_count, refValue, direction, userid, champion_filter, callback) {

    var ret_err;
    var ret_data;
    var matches_list = [];
    log.details("Champion filter: " + champion_filter);
    var sql_template = "select * from(select * from (SELECT *, u.username as username, u.region as region, u.icon_id as icon_id, u.role as role FROM (SELECT *, m.rowid as id FROM match m  left JOIN share_match sh ON m.rowid=sh.match_rowid WHERE m.summoner_id=? ) msh  join user u ON msh.summoner_id = u.summoner_id) {{filter_block}} ORDER BY create_date {{order_direction}}) {{ref_value_block}}  limit ?;";
    log.info("SelectUserMatchesWithUserAndSharingDataAndPaging Fid: " + refValue + " lid: " + direction + ' per page: ' + per_page);
    //db.each(SQL_SELECT_ALL_MATCH_WITH_SHARE_MATCH_DATA_AND_USER_DATA_WITH_PAGING, (paging ? [userid, per_page, (page - 1) * per_page] : [userid, 10, 0]), function (err, row) {
    var sql_query = "";
    var params = [];
    var add_where = "";
    var add_where_option = "";
    params.push(userid);
    if (champion_filter && champion_filter.length > 0) {
        sql_query = sql_template.replace("{{filter_block}}", "where champion_id = ?");
        params.push(champion_filter);
    } else {
        sql_query = sql_template.replace("{{filter_block}}", "");
    }

    if (!refValue || !direction) {
        sql_query = sql_query.replace("{{ref_value_block}}", "").replace("{{order_direction}}", "DESC");
        params.push(per_page);
    } else {

        params.push(refValue);
        params.push(per_page);
        if (direction == 'next') {
            log.info('Direction is next');
            sql_query = sql_query.replace("{{ref_value_block}}", "where create_date < ?").replace("{{order_direction}}", "DESC");
        } else if (direction == 'endRows') {
            log.info('end rows');
            sql_query = sql_query.replace("{{ref_value_block}}", "where create_date >= ?").replace("{{order_direction}}", "ASC");
        } else if (direction == 'startRows') {
            log.info('start rows');
            sql_query = sql_query.replace("{{ref_value_block}}", "where create_date <= ?").replace("{{order_direction}}", "DESC");
        } else {
            log.info('Direction is prev');
            sql_query = sql_query.replace("{{ref_value_block}}", "where create_date > ?").replace("{{order_direction}}", "ASC");
        }

    }
    log.details("SQL: ", sql_query);
    log.details("Params: ", params);
    db.each(sql_query, params, function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        }
        log.info("From database: " + row.id + ': ' + row.match_id, row.create_date, row.summoner_id, JSON.parse(row.items));
        //row.match_details = JSON.parse(row.match_details);
        matches_list.push(row);
    }, function () {

        log.info("Geting all sahred users with rating -----COMPLETE---- ", matches_list.length);
        var isEndReached = false;
        if (direction && direction == 'prev') {
            matches_list.reverse();
            log.info('Page is ' + page);
            var page_count = Math.ceil((rows_count) / per_page)
            log.info('Page count in sql is ' + page_count);
            if (direction == 'endRows') {

            } else {
                //ret_data = matches_list.slice(per_page*(-1));
                //ret_data = matches_list.slice(per_page * (-1));
                //ret_data = matches_list.slice(0, per_page);
            }
            ret_data = matches_list;
        } else if (direction && direction == 'endRows') {
            matches_list.reverse();
            log.info('End rows');
            ret_data = matches_list;
            isEndReached = true;
        } else if (direction && direction == 'startRows') {
            log.info('Start rows');
            ret_data = matches_list;
        } else {
            ret_data = matches_list;
        }
        callback(ret_err, ret_data, isEndReached);
    });
}

module.exports.InsertSharedMatch = InsertSharedMatch;
function InsertSharedMatch(match_rowid, item_order, callback) {
    var ret_err;
    var ret_value;
    var current_time = new Date().getTime();
    log.details("Time: " + current_time + " " + (new Date(current_time)) + " " + item_order);
    var stmt = db.prepare('INSERT INTO share_match (match_rowid, create_time, item_order) VALUES (?, ?, ?)');
    stmt.run(match_rowid, current_time, JSON.stringify(item_order), function (err) {
        if (err) {
            log.error("Unable to insert share_match to database");
            ret_err = "Unable to insert share_match to database";
        }
        callback(ret_err);
    });
    stmt.finalize();
}

module.exports.SelectMatchForSharing = SelectMatchForSharing;
function SelectMatchForSharing(shared_match_rowid, callback) {
    var ret_err;
    var ret_value;
    db.serialize(function () {
        var current_time = new Date().getTime();
        db.get(SQL_SELECT_MATCH_BY_ROW_ID, [shared_match_rowid], function (err, row) {
            if (err) {
                log.error(err);
                ret_err = err;
            } else {
                log.details("Shared match: ", row);
                ret_value = row;
            }

            callback(ret_err, ret_value);
        });
    });

}

module.exports.InsertMatch = InsertMatch;
function InsertMatch(game_id, create_date, summoner_id, win, items, champion_id, callback) {
    var ret_err;
    db.serialize(function () {
        var stmt = db.prepare('INSERT INTO match(match_id, create_date, summoner_id, win, items, champion_id) VALUES (?, ?, ?, ?, ?, ?)');
        stmt.run(game_id, create_date, summoner_id, win, items, champion_id, function (err) {
            if (err) {
                ret_err = err;
            } else {
                log.info("Inserted match: ", game_id);
            }
            callback(ret_err);
        });
        stmt.finalize();
    });
}

module.exports.SelectSummonerChampions = SelectSummonerChampions;
function SelectSummonerChampions(userid, callback) {
    var ret_err;
    var ret_data;
    var params = [];
    var summoner_champions = [];
    params.push(userid);

    db.each(SQL_SELECT_SUMMONER_CHAMPIONS, params, function (err, row) {
        if (err) {
            log.error(err);
            ret_err = err;
        }
        //log.info("From summoner champion: ", JSON.parse(row));

        summoner_champions.push(row);
    }, function () {
        log.details("Complete getting summoner champions");
        ret_data = summoner_champions;
        callback(ret_err, ret_data);
    });
}

module.exports.SelectMatchesWithRating = SelectMatchesWithRating;
function SelectMatchesWithRating(paging, per_page, page, rows_count, refValue, direction, champion_filter, callback) {
    log.details("SQL SelectMatchesWithRating Champion filter: " + champion_filter + " per page: " + per_page + " page: " + page);
    var ret_err;
    var ret_data;
    var matches_list = [];
    var sql_query = "SELECT * FROM (SELECT * FROM (SELECT match_share.*,AVG(r.rating) AS rating,count(r.rating) as rate_count, r.change_time  AS rating_change_time FROM (SELECT *,m.rowid AS id, sh.rowid as share_id FROM match m  JOIN share_match sh ON m.rowid=sh.match_rowid) match_share left JOIN rating r ON match_share.id=r.match_rowid GROUP BY match_share.match_rowid ORDER BY match_share.share_id {{order_direction}}){{filter_block}}) {{ref_value_block}} limit ?;";
    var params = [];

    if (champion_filter && champion_filter.length > 0) {
        sql_query = sql_query.replace("{{filter_block}}", "where champion_id = ?");
        params.push(champion_filter);
    } else {
        sql_query = sql_query.replace("{{filter_block}}", "");
    }

    if (!refValue || !direction) {
        sql_query = sql_query.replace("{{ref_value_block}}", "").replace("{{order_direction}}", "DESC");
        params.push(per_page);
    } else {
        params.push(refValue);
        params.push(per_page);

        if (direction == 'next') {
            log.info('Direction is next');
            sql_query = sql_query.replace("{{ref_value_block}}", "where share_id < ?").replace("{{order_direction}}", "DESC");
        } else if (direction == 'endRows') {
            log.info('end rows');
            sql_query = sql_query.replace("{{ref_value_block}}", "where share_id >= ?").replace("{{order_direction}}", "ASC");
        } else if (direction == 'startRows') {
            log.info('start rows');
            sql_query = sql_query.replace("{{ref_value_block}}", "where share_id <= ?").replace("{{order_direction}}", "DESC");
        } else {
            log.info('Direction is prev');
            sql_query = sql_query.replace("{{ref_value_block}}", "where share_id > ?").replace("{{order_direction}}", "ASC");
        }


    }
    log.details('Champion filter: ', champion_filter);
    log.details("SelectSharedMatches SQL: ", sql_query);
    log.details("SelectSharedMatches Params: ", params);
    db.each(sql_query, params, function (err, row) {
        if (err) {
            log.error("err: ", err);
            log.error(err);
            ret_err = err;
        }

        matches_list.push(row);
    }, function () {

        log.info("Geting all sahred users with rating -----COMPLETE---- ", matches_list.length);
        var isEndReached = false;
        if (direction && direction == 'prev') {
            matches_list.reverse();

            ret_data = matches_list;
        } else if (direction && direction == 'endRows') {
            matches_list.reverse();

            ret_data = matches_list;
            isEndReached = true;
        } else if (direction && direction == 'startRows') {
            log.info('Start rows');
            ret_data = matches_list;
        } else {
            ret_data = matches_list;
        }
        callback(ret_err, ret_data, isEndReached);
    });
}

module.exports.SharedMatchesCount = SharedMatchesCount;
function SharedMatchesCount(champion_filter, callback) {
    var ret_err;
    var ret_data;
    var params = [];
    var sql_query = SQL_COUNT_SHARED_MATCHES_WITH_RATING;
    if (champion_filter && champion_filter.length > 0) {
        sql_query = sql_query.replace("{{filter_block}}", "where champion_id = ?");
        params.push(champion_filter);
    } else {
        sql_query = sql_query.replace("{{filter_block}}", "");
    }
    log.details("Shared matches count sql query: ", sql_query);
    log.details("Shared matches count params: ", params);
    db.get(sql_query, params, function (err, row) {
        if (err) {
            ret_err = err;
            log.error("Error: " + ret_err);
        } else {
            if (row) {
                log.details("SHARED MATCHES COUNT: " + row.matches_count);

                ret_data = row.matches_count;
            } else {
                ret_err = "Failed to get shared matches count from db";
            }
        }
        callback(ret_err, ret_data);
    });
}

module.exports.UpdateMatchRating = UpdateMatchRating;
function UpdateMatchRating(rating, user_summoner_id, match_rid, callback) {
    var ret_err;
    db.serialize(function () {
        var stmt = db.prepare(SQL_UPDATE_RATING_BY_SUMMONER_ID_AND_MATCH_ROWID);
        stmt.run(rating, user_summoner_id, match_rid, function (err) {
            if (err) {
                ret_err = err;
            } else {
                log.info("Updated match(" + match_rid + ")  rating(" + rating + ") by user " + user_summoner_id);
            }
            callback(ret_err);
        });
        stmt.finalize();
    });
}

module.exports.InsertMatchRating = InsertMatchRating;
function InsertMatchRating(user_summoner_id, match_rid, current_time, current_time, rating, callback) {
    var ret_err;
    db.serialize(function () {
        var stmt = db.prepare(SQL_INSERT_MATCH_RATING);
        stmt.run(user_summoner_id, match_rid, current_time, current_time, rating, function (err) {
            if (err) {
                ret_err = err;
            } else {
                log.info("Iserted match(" + match_rid + ")  rating(" + rating + ") by user " + user_summoner_id);
            }
            callback(ret_err);
        });
        stmt.finalize();
    });
}

module.exports.SelectMatchWithRatingByUserIdAndMatchRowId = SelectMatchWithRatingByUserIdAndMatchRowId;
function SelectMatchWithRatingByUserIdAndMatchRowId(user_summoner_id, match_rid, callback) {
    var ret_err;
    var ret_data;
    db.get(SQL_SELECT_MATCH_WITH_RATING_BY_USER_ID_AND_MATCH_ROW_ID, [user_summoner_id, match_rid], HandleSelect);

    function HandleSelect(err, data) {
        callback(err, data)
    }

}

module.exports.SelectMatchBySummonerIdAndMatchId = SelectMatchBySummonerIdAndMatchId;
function SelectMatchBySummonerIdAndMatchId(user_summoner_id, match_rid, callback) {
    var ret_err;
    var ret_data;
    db.get(SQL_SELECT_MATCH_BY_SUMMONER_ID_AND_MATCH_ID, [user_summoner_id, match_rid], HandleSelect);

    function HandleSelect(err, data) {
        callback(err, data);
    }

}

module.exports.InsertNewUserData = InsertNewUserData;
function InsertNewUserData(username, passwordHash, summoner_id, profileIconId, user_region, role, accountId, callback) {
    var ret_err;
    var ret_value;
    
    var stmt = db.prepare(SQL_INSERT_NEW_USER_DATA);
    stmt.run(username, passwordHash, summoner_id, profileIconId, user_region, role, accountId, function (err) {
        if (err) {
            log.error(err);
            ret_err = err;
        }
        callback(ret_err);
    });
    stmt.finalize();
}




