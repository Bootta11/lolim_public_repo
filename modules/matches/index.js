// matches
let express = require('express');
let router = express.Router();
let matches = require('./matches.js');
let paging = require('../paging/paging.js');
let security = require('../security/security.js');
let users = require('../users/users.js');

router.get('/shared_matches/partial', users.loadUsersDataFromSession, paging.preparePaging, matches.getAllSharedMatches, matches.renderSharedMatchesPartial);
router.post('/shared_matches', security.verifyJWTToken, matches.saveSharedMatch);
router.get('/shared_matches', users.loadUsersDataFromSession, paging.preparePaging, matches.getAllSharedMatches, matches.renderSharedMatchesPage);
router.put('/shared_matches/:id/rate', security.verifyJWTToken, matches.rateSharedMatch);
router.get('/shared_matches/:id', security.verifyJWTToken, matches.getSharedMatchById);

router.get('/user_matches/partial', security.verifyJWTToken, paging.preparePaging, matches.getUserMatches, matches.renderUserMatchesPartial);

module.exports = router;