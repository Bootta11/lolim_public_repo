// Filesystem functions
var fs = require('fs');
var config = require('../config.js');
var Response_functions = require('./Response_functions.js');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'REQ_RES_HANDLERS');
const resizeImg = require('resize-img');

function HandleImageResponse(image_response, filePath, return_image_reponse) {

    log.details("sitem request started");
    //response.pipe(file);
    var image = "";
    image_response.setEncoding('binary');
    image_response.on('data', function (data) {
        image += data;
        //file.write(data);
    });
    image_response.on('end', function () {
        log.details("END. Saving item image to file.");
        Response_functions.ReturnPNGImage(return_image_reponse, image);
        
        var image_buffer= new Buffer(image,'binary');
        resizeImg(image_buffer, {width: 64, height: 64}).then(buf => {
            fs.writeFile(filePath, buf, function (err) {
                if (err) {
                    return log.error(err);
                }
                log.details("The item icon image was saved");
            });
        });

//        fs.writeFile(filePath, image, 'binary', function (err) {
//            if (err) {
//                return log.error(err);
//            }
//            log.details("The item icon image was saved");
//        });

    });

}

module.exports.HandleImageResponse = HandleImageResponse;

