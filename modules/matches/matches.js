let config = require('../../config.js');
let SimplogModule = require('../Simplog.js');
let log = new SimplogModule(config.log_level, 'matches');
let sqlite3 = require('sqlite3');
let Redis_functions = require('../Redis_functions.js');
let utils = require('../utils/utils.js');
let Main_Tasks = require('../Main_Tasks.js');
let Sqlite_queries = require('../Sqlite_queries.js');
let async = require('async');

let SQL_SELECT_TOP_SHARE_MATCH_WITH_RATING = 'SELECT match_share.*,AVG(r.rating) AS rating,count(r.rating) as rate_count, r.change_time  AS rating_change_time FROM (SELECT *,m.rowid AS id, sh.rowid as share_id FROM match m  JOIN share_match sh ON m.rowid=sh.match_rowid) match_share JOIN rating r ON match_share.id=r.match_rowid GROUP BY match_share.match_rowid ORDER BY rating DESC LIMIT 5';

function getTopMatches(req, res, next) {
    log.details("Get top matches");
    var stamp = utils.getMinutesFlooredToClosestTens();
    log.details("Stamp generated: " + stamp);
    Redis_functions.GetValue("top:" + stamp, function (d) {
        log.details("Client redis got data");
        if (d && d.length > 0) {
            log.info("Iteminfo redis return top " + stamp + ": " + JSON.parse(d));
            req.top_matches = JSON.parse(d);
            next();
        } else {
            log.info("!Iteminfo redis return top " + stamp + ": " + JSON.parse(d));
            var top_matches = [];
            var db = new sqlite3.Database(config.sqlite_db);
            db.each(SQL_SELECT_TOP_SHARE_MATCH_WITH_RATING, function (err, row) {

                if (err) {
                    log.error("Error: ", err);
                }
                //row.items = JSON.parse(row.items);
                row.item_order = JSON.parse(row.item_order);
                top_matches.push(row);
            }, function () {
                log.details("-----COMPLETE----");
                log.details("top_matches: ", top_matches);
                req.top_matches = top_matches;
                Redis_functions.SetValue("top:" + stamp, JSON.stringify(top_matches), 300);
                next();
            });
            db.close();
        }
    });
}

function getAllSharedMatches(req, res, next) {

    if (req.direction) {
        if (req.direction != 'next') {
            req.page--;
            if (req.page < 1)
                req.page = 1;
        } else {
            req.page++;

        }
    }

    var champ_id = "";
    if (req.body.champion_filter && req.body.champion_filter.length > 0) {
        champ_id = req.body.champion_filter;

    }
    log.details('champ_id: ' + champ_id + ' req champ filter: ', req.champion_filter);
    //log.details("GetAllSharedMatches req.paging: " + req.paging + " req.per_page: " + req.per_page + " req.page: " + req.page + " req.refValue: " + req.refValue + " req.direction: " + req.direction + " champ_id: " + champ_id);
    Main_Tasks.GetSharedMatches(req.paging, req.per_page, req.page, req.refValue, req.direction, champ_id, HandleSharedMatches);

    function HandleSharedMatches(err, count, data, data_page_count, data_page) {

        function HandleAllChampionsData(err, data) {
            if (err) {
                req.all_champions = undefined;
                next();
            } else {
                req.all_champions = JSON.parse(data);
                next();
            }
        }

        if (err || count <= 0) {
            log.details("Count = " + count + ". Error from  HandlePrepareUserMatchesAndMatchesCount ", err);
            req.matches_count = 0;
            req.user_matches = [];
            req.firstRowId = 0;
            Main_Tasks.GetAllChampionsIdNameList((req.session.user ? req.session.user.region : "eune"), HandleAllChampionsData);
        } else {
            log.details('Shared matches: ', data);
            if (count && champ_id) {
                Redis_functions.SetValue('count:shared_matches_with_rating' + (champ_id && champ_id.length > 0 ? "_champion_filter_" + champ_id : ""), count, 30);
            }
            req.page_count = data_page_count;
            req.page = data_page;
            req.matches_count = count;

            req.shared_matches = data;

            if (data && data.length > 0) { // && ((matches.length >= req.per_page) || req.per_page > req.filtered_count )

                req.nextRefValue = data[data.length - 1].share_id;
                req.prevRefValue = data[0].share_id;
                log.details("req.nextRefValue set to: ", req.nextRefValue);
                log.details("req.prevRefValue set to: ", req.prevRefValue);
                log.details("reg Ref value: ", req.refValue);

                Main_Tasks.GetAllChampionsIdNameList((req.session.user ? req.session.user.region : "eune"), HandleAllChampionsData);

            } else {
                //Main_Tasks.PrepareUserMatchesAndMatchesCount(req.paging, req.per_page, p, ref_val, 'endRows', userid, HandlePrepareUserMatchesAndMatchesCount);

                if (req.direction == 'next') {
                    Main_Tasks.GetSharedMatches(req.paging, req.per_page, req.page, req.refValue, 'endRows', champ_id, HandleSharedMatches);
                    //Main_Tasks.PrepareUserMatchesAndMatchesCountWithChampionFilter(req.paging, req.per_page, p, ref_val, 'endRows', userid, champ_id, HandlePrepareUserMatchesAndMatchesCount);
                } else {
                    Main_Tasks.GetSharedMatches(req.paging, req.per_page, req.page + 1, req.refValue, 'startRows', champ_id, HandleSharedMatches);
                    // Main_Tasks.PrepareUserMatchesAndMatchesCountWithChampionFilter(req.paging, req.per_page, p, ref_val, 'startRows', userid, champ_id, HandlePrepareUserMatchesAndMatchesCount);
                }
            }

        }
    }
}

function getUserMatches(req, res, next) {

    var userid = req.params.userid;
    if (!userid) {
        if (req.body.summoner_id) {

            userid = req.body.summoner_id;
        } else if (req.query.summoner_id) {
            userid = req.query.summoner_id;
        } else {
            userid = req.session.user.summoner_id;
            if (!userid) {
                res.render("pages/error", {message: 'Valid user not found'});
                return;
            }
        }
    }


    log.details("Before redis client. Page: " + req.page + " per page: " + req.per_page + ' ref: ' + req.refValue + ' next: ' + req.direction);
    var ref_val = req.refValue;
    //var inv_dir = (req.direction == 'true' ? 'false' : 'true');
    var p = req.page;
    if (req.direction) {
        if (req.direction != 'next') {
            req.page--;
            if (req.page < 1)
                req.page = 1;
        } else {
            req.page++;

        }
    }
    var champ_id = "";
    if (req.query.champion_filter && req.query.champion_filter.length > 0) {
        champ_id = req.query.champion_filter;

    }
    log.details('champ_id: ' + champ_id + ' req cahmp filter: ', req.champion_filter);


    //Main_Tasks.PrepareUserMatchesAndMatchesCount(req.paging, req.per_page, req.page, ref_val, req.direction, userid, HandlePrepareUserMatchesAndMatchesCount);
    Main_Tasks.PrepareUserMatchesAndMatchesCountWithChampionFilter(req.paging, req.per_page, req.page, req.refValue, req.direction, userid, champ_id, HandlePrepareUserMatchesAndMatchesCount);

    function HandlePrepareUserMatchesAndMatchesCount(err, count, matches, win_count, filtered_count) {
        if (err) {

            log.error("Error from  HandlePrepareUserMatchesAndMatchesCount ", err);
            req.matches_count = 0;
            req.user_matches = [];
            req.firstRowId = 0;
            next();
        } else {

            log.details('From HandlePrepareUserMatchesAndMatchesCount matches count: ' + count + ' win count: ' + win_count + ' before req setting, filtered count: ' + filtered_count);
            if (filtered_count) {
                req.matches_count = filtered_count;
            } else {
                req.matches_count = count;
            }
            //req.matches_count = count;
            req.user_matches = matches;
            req.win_count = win_count;
            req.filtered_count = filtered_count;

            if (matches && matches.length > 0) { // && ((matches.length >= req.per_page) || req.per_page > req.filtered_count )

                log.details('Navigation: refValue: ' + req.refValue + ' direction: ' + req.direction);
                var page_count = Math.ceil((req.filtered_count && req.filtered_count > 0 ? req.filtered_count : req.matches_count) / req.per_page);
                req.page_count = page_count;
                if (req.direction) {
                    if (req.direction == 'next') {
                        // req.page++;


                        log.details("Page count: " + page_count);
                        if (req.page > page_count)
                            req.page = page_count;
                    }
                }
                req.nextRefValue = matches[matches.length - 1].create_date;
                log.details('req.nextRefValue: ', req.nextRefValue);
                log.details('req.prevRefValue', req.prevRefValue)
                req.prevRefValue = matches[0].create_date;

                Main_Tasks.GetSummonerChamopionsWithNames((req.session.user ? req.session.user.region : "eune"), userid, HandleUserChampionsData);

                function HandleUserChampionsData(err, data) {
                    if (err) {
                        req.user_champions = undefined;
                        next();
                    } else {
                        req.user_champions = data;
                        next();
                    }
                }

            } else if (count > 0) {

                //Main_Tasks.PrepareUserMatchesAndMatchesCount(req.paging, req.per_page, p, ref_val, 'endRows', userid, HandlePrepareUserMatchesAndMatchesCount);

                if (req.direction == 'next') {
                    Main_Tasks.PrepareUserMatchesAndMatchesCountWithChampionFilter(req.paging, req.per_page, p, ref_val, 'endRows', userid, champ_id, HandlePrepareUserMatchesAndMatchesCount);
                } else {
                    Main_Tasks.PrepareUserMatchesAndMatchesCountWithChampionFilter(req.paging, req.per_page, p, ref_val, 'startRows', userid, champ_id, HandlePrepareUserMatchesAndMatchesCount);
                }
            } else {
                next();
            }

        }
    }

}

function getSharedMatchById(req, res) {
    var id = req.params.id;

    Sqlite_queries.SelectMatchForSharing(id, HandleMatchForSharing);

    function HandleMatchForSharing(err, shared_match) {
        if (err) {
            res.render('partials/share_match', {match: undefined});
        } else {
            shared_match.items = JSON.parse(shared_match.items);
            res.render('partials/share_match', {match: shared_match});
        }
    }
}

function saveSharedMatch(req, res) {
    var id = req.body.id;
    if (!req.body.item_order || req.body.item_order.length <= 0) {
        res.json({success: false, message: "Missing input data"});
        return;
    }
    var item_order = JSON.parse(req.body.item_order);

    Sqlite_queries.InsertSharedMatch(id, item_order, HandleInsertSharedMatch);

    function HandleInsertSharedMatch(err) {
        if (err) {
            res.json({success: false, message: err});
            return;
        } else {
            res.json({success: true});
        }
    }

}

function rateSharedMatch(req, res) {

    var current_time = new Date().getTime();
    var user_summoner_id = req.session.user.summoner_id;
    var rating = req.body.rating;
    var match_rid = req.params.id;
    log.details("curr_time: " + current_time + " user_id: " + user_summoner_id + " ratig: " + rating + " match_rid: " + match_rid);

    var rate_match = async.seq(
            function (done) {
                Sqlite_queries.SelectMatchWithRatingByUserIdAndMatchRowId(user_summoner_id, match_rid, done)
            },
            function (match, done) {
                if (match) {
                    Sqlite_queries.UpdateMatchRating(rating, user_summoner_id, match_rid, HandleUpdateMatchRating);
                    function HandleUpdateMatchRating(err) {
                        if (err) {
                            done("Unable to update rating in database");
                        } else {
                            done(null, "Match rating updated");
                        }
                    }
                } else {
                    Sqlite_queries.InsertMatchRating(user_summoner_id, match_rid, current_time, current_time, rating, HandleInsertMatchRating);
                    function HandleInsertMatchRating(err) {
                        if (err) {
                            done("Unable to insert rating to database")
                        } else {
                            done(null, "Match rated");
                        }
                    }
                }
            }
    );

    rate_match(HandleRateMatch);

    function HandleRateMatch(err, result) {
        if (err) {
            log.error("Error: ", err);
            res.json({success: false, message: err});
        } else {
            res.json({success: true, message: result});
        }
    }


}

function renderSharedMatchesPage(req, res) {

    log.details("NUMBER OF PAGES /render/user_matches/: " + req.page_count + " " + req.matches_count + " " + req.per_page);
    //res.render('pages/matches', {matches: req.user_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, direction: req.direction, matches_count: req.matches_count, win_count: req.win_count, user_champions: req.user_champions, champion_filter: req.body.champion_filter});

    res.render('pages/shared_matches', {shared_matches: req.shared_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, all_champions: req.all_champions, champion_filter: req.body.champion_filter});
}

function renderSharedMatchesPartial(req, res) {
    log.details('renderSharedMatchesPartial');

    res.render('partials/show_matches_with_rating', {shared_matches: req.shared_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, all_champions: req.all_champions, champion_filter: req.query.champion_filter});
}

function renderUserMatchesPartial(req, res) {

    log.info('Render user matches post. Summonerid: ' + req.query.summoner_id + ' perpage: ' + req.query.per_page + ' ref: ' + req.query.refValue + ' direction: ' + req.query.direction + " champion_filter: " + req.query.champion_filter);
    log.details("NUMBER OF PAGES /render/user_matches/: " + Math.ceil(req.matches_count / req.per_page) + " " + req.matches_count + " " + req.per_page);
    log.details('From api post /render/user_matches. matches count: ' + req.matches_count + ' win count: ' + req.win_count);
    res.render('partials/show_user_matches', {matches: req.user_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, direction: req.direction, matches_count: req.matches_count, win_count: req.win_count, user_champions: req.user_champions, champion_filter: req.query.champion_filter});
}

module.exports = {
    getTopMatches: getTopMatches,
    getAllSharedMatches: getAllSharedMatches,
    getUserMatches: getUserMatches,
    getSharedMatchById: getSharedMatchById,
    saveSharedMatch: saveSharedMatch,
    rateSharedMatch: rateSharedMatch,
    renderSharedMatchesPage: renderSharedMatchesPage,
    renderSharedMatchesPartial: renderSharedMatchesPartial,
    renderUserMatchesPartial: renderUserMatchesPartial
};