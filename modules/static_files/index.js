// static
let express = require('express');
let router = express.Router();
let static_files = require('./static_files.js');
let paging = require('../paging/paging.js');
let security = require('../security/security.js');
let matches = require('../matches/matches.js');

router.get('/image/:type/:id', static_files.getImage)

module.exports = router;