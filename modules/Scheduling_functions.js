// Scheduling functions
var schedule = require('node-schedule');
var parsing = require('./Parsing.js');
var sqlite3 = require('sqlite3');
var config = require('../config.js');
var Main_Tasks = require('./Main_Tasks.js');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level,'SCHEDULING_JOB');

var SQL_SELECT_ALL_USERS = 'SELECT * FROM user';

function updatingJob() {
    var users_per_cycle = 10;
    var users = [];
    var db = new sqlite3.Database(config.sqlite_db);

    db.serialize(function () {

        db.each(SQL_SELECT_ALL_USERS, function (err, row) {

            users.push(row);
        }, function () {
            log.details("-----COMPLETE----");
            for (var i = 0; i <= (Math.floor(users.length / users_per_cycle)); i++) {
                var users_sliced = users.slice(i * users_per_cycle, (i + 1) * users_per_cycle);
                for (var s = 0; s < users_sliced.length; s++) {
                    Main_Tasks.CheckForUpdateSummonerData(users_sliced[s].region, users_sliced[s].summoner_id, function (err) {
                        if (err) {
                            log.error("StartSchedulingJob error: " + err);
                        }
                    });
                    parsing.ParseAndStoreRecentMatches(users_sliced[s].region, users_sliced[s].summoner_id, users_sliced[s].account_id);
                }
            }

        });


    });

    db.close();
}

var StartSchedulingJob = function () {


    log.details("Scheduling jobs");
    var job_hour00 = schedule.scheduleJob('0 * * * *', function () {
        log.details('JOB 00 running');

        updatingJob();
    });

    var job_hour15 = schedule.scheduleJob('15 * * * *', function () {
        log.details('JOB 15 running');

        updatingJob();
    });

    var job_hour30 = schedule.scheduleJob('30 * * * *', function () {
        log.details('JOB 30 running');

        updatingJob();
    });

    var job_hour45 = schedule.scheduleJob('45 * * * *', function () {
        log.details('JOB 45 running');

        updatingJob();
    });

    var job_hourXX = schedule.scheduleJob('53 * * * *', function () {
        log.details('JOB XX running'); 

        updatingJob();
    });


};

module.exports.StartSchedulingJob = StartSchedulingJob;

