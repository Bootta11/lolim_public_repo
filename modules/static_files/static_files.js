const config = require('../../config.js');
const SimplogModule = require('../Simplog.js');
const log = new SimplogModule(config.log_level, 'static_files');
const compressor = require('node-minify');
var fs = require('fs');
var utils_functions = require('../Util_functions.js');
var FS_Functions = require('../FS_Functions.js');
var Response_functions = require('../Response_functions.js');
var http = require('http');

function minifyJS() {
    compressor.minify({
        compressor: 'uglifyjs',
        input: __dirname + '/../../public/js/func.js',
        output: __dirname + '/../../public/js/func.min.js',
        sync: true,
        options: {
            warnings: true, // pass true to display compressor warnings. 
            mangle: true, // pass false to skip mangling names. 
            compress: true // pass false to skip compressing entirely. Pass an object to specify custom compressor options. 
        },
        callback: function (err, min) {
            if (err) {
                log.details("func.js minifying error");
            }
            log.details("func.js minified");

        }
    });
}

function getImage(req, res) {
    res.setHeader('Cache-Control', 'public, max-age=3600');
    //extract parameters
    var id = req.params.id;
    var type = req.params.type;
    var region = (req.session.user ? req.session.user.region : "eune");

    if (type == 'champion') {

        utils_functions.GetChampionKeyById((req.session.user ? req.session.user.region : "eune"), id, HandleGetChampionKey);

        function HandleGetChampionKey(err, name) {

            if (!err) {
                id = name;

            } else {
                log.details("Handle ch name id: " + id + " type: " + type + " err: " + err);
            }

            GetImage(type, id);
        }
    } else {
        GetImage(type, id);
    }
    log.details("Getting image for type: " + type + " and id: " + id);

    function GetImage(type, id) {
        //Check and create file path
        var image_folder = 'public/images/temp/' + type;
        if (!fs.existsSync(image_folder)) {
            try {
                fs.mkdirSync(image_folder);
            } catch (err) {
                log.error(err);
            }
        }


        if (!fs.existsSync(image_folder)) {
            fs.mkdirSync(image_folder);
        }
        var filePath = image_folder + '/' + id + '.png';
        //If image exist localy return it if not than download it and return it
        if (!fs.existsSync(filePath)) {
            log.details("Downloading and saving icon " + filePath);

            utils_functions.GetLatestVersion(region, function (err, data) {
                if (err) {
                    var err_msg = 'Get image - unable to get current versions list';
                    log.error(err_msg);

                } else {
                    var latest_version = data;


                    var request_image_url = config.urls.static.images.replace('{{type}}', type).replace('{{id}}', id).replace('{{version}}', latest_version);
                    log.details('Rquest iamge url: ', request_image_url);
                    var requestItemImage = http.get(request_image_url, function (response) {
                        if (response.statusCode == '200' || response.statusCode == '302') {
                            FS_Functions.HandleImageResponse(response, filePath, res);
                        } else {
                            log.error("Image not found");
                            Response_functions.ReturnPNGImage(res, "");
                        }
                    }).on('error', function (err) {
                        log.error(err);
                    });
                }
            });


        } else {
            log.details("Sending existing item image icon");
            //working
            var img = fs.readFileSync(filePath);
            Response_functions.ReturnPNGImage(res, img);
        }
    }


}

module.exports = {
    minifyJS: minifyJS,
    getImage: getImage
};