let sqlite3 = require('sqlite3');
let config = require('../../config.js');

var User = function (data) {
    this.data = data;
    
};

User.prototype.data = {};
User.prototype.logged="";
User.current=null;

User.prototype.changeName = function (name) {
    this.data.name = name;
};

User.findByName = function (name, callback) {

    var db = new sqlite3.Database(config.sqlite_db);
    db.serialize(function () {

        db.get('SELECT *  FROM user WHERE username=?', name, function (err, row) {
            if (err)
                return callback(err);
            User.current=new User(row);
            callback(null, User.current);

        });


    });

    db.close();

    
};

module.exports = User;