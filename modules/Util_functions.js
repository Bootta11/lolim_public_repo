// Util functions
var config = require('../config.js');
var main_tasks = require('./Main_Tasks.js');
var FS_Functions = require('./FS_Functions.js');
var Redis_functions = require('./Redis_functions');
var Response_functions = require('./Response_functions.js');
var API_requests = require('./API_requests');
var SimplogModule = require('./Simplog.js');
var log = new SimplogModule(config.log_level, 'UTILS');

var async = require('async');



module.exports.GetChampionKeyById = GetChampionKeyById;
function GetChampionKeyById(region, id, callback) {
    main_tasks.GetAllChampionsInfo(region, HandleChampionsData);
    var ret_err;
    var ret_data;

    function HandleChampionsData(err, data) {
        if (err) {
            return id;
        } else {
            var json_data = JSON.parse(data);
            var champ;
            var champ_key;
            try {
                champ = json_data.data[id];
            } catch (err) {

            }

            if (champ) {
                champ_key = champ.key;
            }

            if (champ_key) {
                ret_data = champ_key;
            } else {
                ret_err = "Not found";
            }

            callback(ret_err, ret_data);
        }
    }
}

module.exports.GetChampionNameById = GetChampionNameById;
function GetChampionNameById(region, id, callback) {
    main_tasks.GetAllChampionsInfo(region, HandleChampionsData);
    var ret_err;
    var ret_data;

    function HandleChampionsData(err, data) {
        if (err) {
            return id;
        } else {
            var json_data = JSON.parse(data);
            var champ;
            var champ_name;
            try {
                champ = json_data.data[id];
            } catch (err) {

            }
            log.details(champ);
            if (champ) {
                champ_name = champ.name;
            }
            log.details(champ_name);
            if (champ_name) {
                ret_data = champ_name;
            } else {
                ret_err = "Not found";
            }
            log.details("Returning champ name: ", ret_data);
            callback(ret_err, ret_data);
        }
    }
}

module.exports.GetLatestVersion = GetLatestVersion;
function GetLatestVersion(region, callback) {
    
    log.info('GetAllChampionsInfo')
    GetApiData(region,'latest_version',API_requests.RequestCurrentVersion,callback);
//    var ret_err;
//    var ret_data;
//
//    var fn = async.seq(
//            function ReadLatestVersionAcquiredValueFromRedis(done) {
//                log.info(' Step ReadLatestVersionAcquiredValueFromRedis');
//                Redis_functions.GetValue('latest_version_acquired_'+region, HandleLatestVersionAcquiredValue);
//
//                function HandleLatestVersionAcquiredValue(acquired_data) {
//                    done(null, acquired_data);
//                }
//            },
//            function AcquireNewDataIfRequired(acquired, done) {
//                log.info('Step AcquireNewDataIfRequired');
//                if (!acquired) {
//                    log.details('Acquire latest version data from api');
//                    API_requests.RequestCurrentVersion(region, HandleLatestVersionData);
//
//                    var ret_data;
//                    function HandleLatestVersionData(err, data) {
//                        if (!err) {
//                            ret_data = data;
//                            Redis_functions.SetValue('latest_version_'+region, data);
//                            Redis_functions.SetValue('latest_version_acquired_'+region, 'DONE', 60 * 60 * 24);
//                        }
//                        done(null);
//                    }
//                } else {
//                    log.details('Latest version already acquired');
//                    done(null);
//                }
//            },
//            function ReadLatestVersionFromRedis(done) {
//                log.info('Step ReadLatestVersionFromRedis');
//
//                Redis_functions.GetValue("latest_version_" + region, HandleLatestVersionDataFromRedis);
//
//                function HandleLatestVersionDataFromRedis(data) {
//                    if (!data) {
//                        API_requests.RequestCurrentVersion(region, HandleLatestVersionData);
//                        function HandleLatestVersionData(err, data) {
//                            if (err) {
//
//                                ret_err = err;
//                            } else {
//                                ret_data = data;
//                                Redis_functions.SetValue("latest_version_" + region, data);
//                                Redis_functions.SetValue("latest_version_acquired_" + region, 'DONE', 60 * 60 * 24);
//                            }
//                            done(ret_err, ret_data);
//                        }
//
//                    } else {
//                        ret_data = data;
//
//                        done(ret_err, ret_data);
//                    }
//                }
//
//            }
//
//    );
//
//    fn(callback);
    
    
//    console.log('~~~~~~~~~~~~~~~get altest version')
//    Redis_functions.GetValue('latest_version_'+region, function (data) {
//        if (data && data.length > 0) {
//            console.log('~~~~~~~~~~~~~~~data length 0');
//            callback(null, data);
//        } else {
//            console.log('~~~~~~~~~~~~~~~No version on redis. Geting from api');
//            API_requests.RequestCurrentVersion(region, function (err, data) {
//                console.log('~~~~~~~~~~~~~~~Requesting version from api err: ',err, ' data: ',data);
//                if (err) {
//                    var err_msg = 'Get image - unable to get current versions list';
//                    log.error(err);
//                    callback(err_msg);
//                } else {
//                    try {
//                        var latest_version = JSON.parse(data)[0];
//                        if (latest_version) {
//                            Redis_functions.SetValue('latest_version_'+region, latest_version);
//                            Redis_functions.SetValue('latest_version_acquired_'+region, 'done', 60 * 10);
//
//                            callback(null, latest_version);
//                        } else {
//                            callback('Invalid version data');
//                        }
//                    } catch (ex) {
//                        callback(ex);
//                    }
//                }
//            });
//        }
//    });



}

module.exports.GetApiData = GetApiData;
function GetApiData(region,redis_data_name, api_function, callback) {
    
    log.info('GetApiData')
    var ret_err;
    var ret_data;

    var fn = async.seq(
            function ReadDataAcquiredValueFromRedis(done) {
                log.info(' Step ReadDataAcquiredValueFromRedis');
                Redis_functions.GetValue(redis_data_name+'_acquired_'+region, HandleDataAcquiredValue);

                function HandleDataAcquiredValue(acquired_data) {
                    done(null, acquired_data);
                }
            },
            function AcquireNewDataIfRequired(acquired, done) {
                log.info('Step AcquireNewDataIfRequired');
                if (!acquired) {
                    log.details('Acquire ['+redis_data_name+'] data from api');
                    api_function(region, HandleApiData);

                    var ret_data;
                    function HandleApiData(err, data) {
                        if (!err) {
                            ret_data = data;
                            Redis_functions.SetValue(redis_data_name+'_'+region, data);
                            Redis_functions.SetValue(redis_data_name+'_acquired_'+region, data, 60 * 60 * 24);
                        }
                        done(null,data);
                    }
                } else {
                    log.details('Api data ['+redis_data_name+'] already acquired');
                    done(null,acquired);
                }
            },
            function ReadDataFromRedis(data, done) {
                log.info('Step ReadDataFromRedis');
                
                if(!data){
                    ret_err = 'No data ['+redis_data_name+']. Unable to get it from redis or api.';
                }
                
                ret_data = data;
                
                done(ret_err, ret_data);


            }

    );

    fn(callback);


}











