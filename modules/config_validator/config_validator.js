const Joi = require('joi');

function validateConfigFile(config,config_validation_schema) {
    
    const config_validation_result = Joi.validate(config, config_validation_schema);
    if (config_validation_result.error) {
        const error_details = config_validation_result.error.details;

        let validation_errors_list = '';
        for (let i = 0; i < error_details.length; i++) {
            validation_errors_list += error_details[i].message + (i < error_details.length - 1 ? ', ' : '');
        }
        
        throw new Error("Application config is not valid. Validation error: " + validation_errors_list);
    }
}

module.exports = {
    validateConfigFile: validateConfigFile
};