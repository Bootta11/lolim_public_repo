var winston=require('winston');
require('winston-loggly-bulk');

winston.add(winston.transports.Loggly, {
    token: "5bc326b6-45f6-4fba-835f-b2851f42db20",
    subdomain: "bootta",
    tags: ["Winston-NodeJS"],
    json:true
});

// Simple log
module.exports = Simplog;
function Simplog(ll, log_prefix = '') {
    this.log_level = ll;
    this.prefix = log_prefix;
    console.log('Created Simplog with log level: ', this.log_level, ', prefix: ', this.prefix);
}
;

Simplog.prototype.details = details;
function details() {
    var log_level_param = 'verbose';
    if (this.log_level && (this.log_level == 'details')) {
        HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.info = info;
function info() {
    var log_level_param = 'info';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info')) {
        HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.warn = warn;
function warn() {
    var log_level_param = 'warn';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info' || this.log_level == 'warn')) {
        HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

Simplog.prototype.error = error;
function error() {
    var log_level_param = 'error';
    if (this.log_level && (this.log_level == 'details' || this.log_level == 'info' || this.log_level == 'warn' || this.log_level == 'error')) {
        HandleArgumentListAndLogToConsole(arguments, this.prefix, log_level_param);
    }
}

function HandleArgumentListAndLogToConsole(arguments, prefix, log_level_param) {
    var log_prefix_label = (prefix ? '[' + prefix + '] ' : '');

    if (arguments.length > 0) {
        if (arguments.length > 1) {
            var obj;
            try {
                obj = JSON.parse(arguments[1]);
            } catch (e) {
                obj = arguments[1];
            }
            //console.log(log_prefix_label, arguments[0], obj);
            winston.log(log_level_param, arguments[0], obj);
        } else {
            //console.log(log_prefix_label, arguments[0]);
            winston.log(log_level_param, arguments[0]);
        }
    }
}