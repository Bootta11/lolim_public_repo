// items
let express = require('express');
let router = express.Router();
let matches = require('../matches/matches.js');
let paging = require('../paging/paging.js');
let security = require('../security/security.js');
let items = require('./items.js')

router.get('/item_info/:itemid', items.getItemInfo);

module.exports = router;