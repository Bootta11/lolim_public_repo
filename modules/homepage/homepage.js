let config = require('../../config.js');
let SimplogModule = require('../Simplog.js');
let log = new SimplogModule(config.log_level, 'homepage');
let path = require('path');

 function renderHomepage(req, res) {
    log.info('Loading homepage');
    log.info(path.join(__dirname, 'homepage.ejs'));
    res.render('pages/homepage', {shared_matches: req.shared_matches, top_matches: req.top_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, all_champions: req.all_champions});
    return;
}

module.exports = {
    renderHomepage: renderHomepage
};