const Joi = require('joi');

const schema = Joi.object().keys({
    'port':Joi.number().integer().greater(1024).required(),
    'log_level':Joi.string().required(), 
    'secret': Joi.string().required(),
    'session_secret': Joi.string().required(),
    'password_hash_rounds': Joi.number().integer().positive().required(),
    'token_expire_in': Joi.number().integer().positive().required(),
    'sqlite_db': Joi.string().required(),
    'regions':Joi.object().required(),
    'riot_api_base_url': Joi.required(),
    'api_token': Joi.string().required(),
    'urls': Joi.object().keys({
        static: Joi.object().keys({
            versions: Joi.string().required(),
            item: Joi.string().required(),
            items: Joi.string().required(),
            champions: Joi.string().required(),
            images: Joi.string().required()
        }),
        api: Joi.object().keys({
            user_recent_matches: Joi.string().allow('').required(),
            user_data_by_summoner_id: Joi.string().required(),
            recent_matches: Joi.string().required(),
            match_data: Joi.string().required(),
            summoner_data_by_name: Joi.string().required()
        })
    })
});

module.exports = schema;

