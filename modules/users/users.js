let config = require('../../config.js');
let SimplogModule = require('../Simplog.js');
let log = new SimplogModule(config.log_level, 'users');
let sqlite3 = require('sqlite3');
let bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');
let Sqlite_queries = require('../Sqlite_queries.js');
let parsing = require('../Parsing.js');
let API_requests = require('../API_requests.js');
let utils = require('../utils/utils.js');
let User = require('./user_model.js');

let SQL_SELECT_ALL_USERS = 'SELECT * FROM user';

function loadUsersDataFromSession(req, res, next) {
    res.locals.user = req.session.user;
    next();
}
;

function renderUserMatchesPage(req, res) {
    log.details('From api get /user/:region/:userid. matches count: ' + req.matches_count + ' win count: ' + req.win_count);
    res.render('pages/matches', {userid: req.params.userid, matches: req.user_matches, per_page: req.per_page, page: req.page, number_of_pages: Math.ceil(req.matches_count / req.per_page), prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, direction: req.direction, matches_count: req.matches_count, win_count: req.win_count, user_champions: req.user_champions, champion_filter: req.body.champion_filter});
}

function renderUsersPage(req, res) {
    var users = [];
    var db = new sqlite3.Database(config.sqlite_db);
    db.serialize(function () {

        db.each(SQL_SELECT_ALL_USERS, function (err, row) {

            users.push(row);
        }, function () {
            log.details("-----COMPLETE----");

            res.render('pages/users', {users: users});
            return;
        });
    });
    db.close();
}

function renderLoggedUserMatchesPage(req, res) {

    log.info("/user/:userid");
    var summoner_id = req.session.user.summoner_id;
    var region = req.session.user.region;
    var account_id = req.session.user.account_id;
    log.details("Parse recent matches for user " + summoner_id);
    var content = "";
    parsing.ParseAndStoreRecentMatches(region, summoner_id, account_id);
    log.details('First row ref: ' + req.refValue + ' direction: ' + req.direction);
    log.details('From api get /user/matches. matches count: ' + req.matches_count + ' win count: ' + req.win_count);
    res.render('pages/matches', {matches: req.user_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count, prevRefValue: req.prevRefValue, nextRefValue: req.nextRefValue, direction: req.direction, matches_count: req.matches_count, win_count: req.win_count, user_champions: req.user_champions, champion_filter: req.body.champion_filter});
}

function getUserInfoByName(req, res) {
    //res.send("User info for "+req.params.name);
    log.details("Info for region " + req.params.region + " name " + req.params.name);
    var region = req.params.region;
    var summoner_name = req.params.name;


    API_requests.RequestSummonerDataByName(region, summoner_name, function (error, data) {
        if (error) {
            var error_message = 'Error: Unable to get user dat by name ' + summoner_name;
            log.error(error_message);

            return res.send(error_message);
        } else {
            return res.send(JSON.parse(data));
        }
    });
}

function renderUserMatchesPartial(req, res) {

    log.details("NUMBER OF PAGES /render/user_matches/: " + req.page_count + " " + req.matches_count + " " + req.per_page);
    res.render('partials/show_user_matches', {matches: req.user_matches, per_page: req.per_page, page: req.page, number_of_pages: req.page_count});
}

function userSignup(req, res) {
    res.render('pages/signup');
}

function userLogin(req, res) {
    res.render('pages/login');
}

function userLogout(req, res) {
    req.session.destroy(function () {
        log.info("Session destroyed");
    });
    res.redirect('/');
}

function userSignupProcess(req, res) {
    var summoner_name = req.body.inputSummonerName;
    var region = req.body.inputRegion;

    API_requests.RequestSummonerDataByName(region, summoner_name, function (error, data) {
        if (error) {
            log.error(error);
            return;
        }
        let user_plain_password = utils.escapeHtml(req.body.inputPassword);
        let user_info = JSON.parse(data);
        let role = 'normal';
        log.details("Signup User getted info process", user_info);

        bcrypt.hash(user_plain_password, config.password_hash_rounds, HandlePasswordHash);

        function HandlePasswordHash(err, hashed_password) {
            if (err) {
                log.error(err);

                let error_message = 'Unable to hash user password';

                return res.render("pages/error", {message: error_message});
            }

            Sqlite_queries.InsertNewUserData(utils.escapeHtml(req.body.inputUsername), hashed_password, user_info.id, user_info.profileIconId, utils.escapeHtml(req.body.inputRegion), role, user_info.accountId, HandleInsertUserData);

            function HandleInsertUserData(err) {
                if (err) {
                    var error_message = 'Unable to insert user to database';

                    if (err && err.code == 'SQLITE_CONSTRAINT') {
                        error_message = 'Unable to insert user to database. Probably summoner id or username already exist.';
                    }
                    log.error(error_message);
                    return res.render("pages/error", {message: error_message});
                }

                return res.redirect('/login');

            }

        }

    });
}

function userAuthenticate(req, res) {

    User.findByName(req.body.inputUsername, function (err, user) {
        console.log('Username: ', req.body.inputUsername, ' err: ', err, ' user: ', user);
        if (err) {
            log.error(err);
            throw err;
        }

        if (!user.data) {
            log.error("User not defined");
            res.render('pages/error', {message: 'Authentication failed. User not found.'});
            //res.json({success: false, message: 'Authentication failed. User not found.'});
        } else if (user.data) {
            log.details("User exist: ", user.data);
            // check if password matches

            bcrypt.compare(req.body.inputPassword, user.data.password, HandlePasswordCompare);

            function HandlePasswordCompare(err, password_match) {
                console.log('HandlePasswordCompare');
                if (!password_match) {
                    return res.render('pages/error', {message: 'Authentication failed. Wrong password.'});
                }

                // if passwords match create token
                var token = jwt.sign(user, config.secret, {
                    expiresIn: config.token_expire_in // expires in 24 hours
                });

                // return user and token data
                req.session.user = user.data;
                req.session.token = token;

                res.redirect('/users/logged_user_matches');
            }

        } else {
            log.error("No user data");
            res.render('pages/error', {message: 'Authentication failed. Unknown error.'});
        }
        log.details("User by id: ", user);
    });
}

module.exports = {
    loadUsersDataFromSession: loadUsersDataFromSession,
    renderUserMatchesPage: renderUserMatchesPage,
    renderUsersPage: renderUsersPage,
    renderLoggedUserMatchesPage: renderLoggedUserMatchesPage,
    getUserInfoByName: getUserInfoByName,
    renderUserMatchesPartial: renderUserMatchesPartial,
    userSignup: userSignup,
    userLogin: userLogin,
    userLogout: userLogout,
    userSignupProcess: userSignupProcess,
    userAuthenticate: userAuthenticate
};


