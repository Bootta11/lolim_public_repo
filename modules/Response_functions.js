// Response functions

function ReturnPNGImage(response, img) {
    response.writeHead(200, {'Content-Type': 'image/png'});
    response.end(img, 'binary');
}

module.exports.ReturnPNGImage = ReturnPNGImage;

