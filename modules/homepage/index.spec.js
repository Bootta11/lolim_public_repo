let chai = require('chai');
let expect = chai.expect;
let should = chai.should();
let assert = chai.assert;
let Request = require('request');

describe('homepage', function () {
    let server;

    before(function () {
        this.timeout(10000);
        server = require('../../app.js');
    });

    after(function () {
        server.close();
    });

    describe('GET /', function () {
        let data = {};
        before(function (done) {
            Request.get('http://localhost:3000', {timeout: 20000}, function (error, response, body) {
                if (!error) {
                    data.status = response.statusCode;
                    data.body = body;
                }
                done();
            });
        });

        it('Return status 200', function () {
            expect(data.status).to.equal(200);
        });
    });
});