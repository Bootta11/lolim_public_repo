let jwt = require('jsonwebtoken');
let config = require('../../config.js');
let SimplogModule = require('../Simplog.js');
let log = new SimplogModule(config.log_level, 'security');

function verifyJWTToken(req, res, next) {

    var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.session.token;
    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                req.session.destroy(function () {
                    log.details("Session destroyed");
                });
                res.status(403).render('pages/error', {message: 'Failed to authenticate token.'});
                //return res.json({success: false, message: 'Failed to authenticate token.'});
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {

        // if there is no token
        // return an error
        req.session.destroy(function () {
            log.details("Session destroyed");
        });
       
        res.status(403).render('pages/error', {message: 'No valid authorization.'});
    }
};

module.exports = {
    verifyJWTToken: verifyJWTToken
};